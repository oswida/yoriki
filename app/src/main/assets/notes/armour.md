# Charakterystyki zbroi
@rules

| Nazwa                                | Bonus do obrony |
|--------------------------------------|:---------------:|
| Futro, peleryna futrzana             |        2        |
| Miękka skóra, gruba tkanina          |        2        |
| Pikowany jedwab                      |        2        |
| Kość i skóra                         |       1K        |
| Wzmocniona skóra                     |       1K        |
| Ciężka skóra                         |      1K+1       |
| Kolczuga                             |      1K+2       |
| Zbroja Ashigaru                      |       1K        |
| Lekka zbroja                         |      1K+1       |
| Ciężka zbroja                        |       2K        |
| Pełna ciężka zbroja z hełmem i maską |       3K        |
| Zbroja jeździecka                    |      1K+2       |
