# Bitwy
@rules

## Runda bitwy
Runda bitwy będzie trwała od 30 minut do 1 godziny, w zależności od potrzeb GM. W tym czasie bohaterowie będą w środku walk, nie robiąc nic więcej. Po każdej rundzie określisz, czy bitwa jest zwycięstwem, porażką, czy też jest wciąż nierozstrzygnięta. W tym ostatnim przypadku walka toczy się dalej o kolejną rundę.

## Poziom zaangażowania
Przed rozstrzygnięciem rundy bitwy, każda postać musi wybrać poziom zaangażowania:
- **Niezaangażowana:** postać nie jest częścią bitwy, nie ma nic do zrobienia.
- **Wsparcie:** postać pozostaje w tyle, aby dbać o drobne zadania, takie jak komenda (co nie jest zadaniem drugorzędnym), zaopatrzenie, komunikacja, opieka medyczna, wsparcie artyleryjskie. Nie będzie ryzykował kłopotów, z wyjątkiem straszliwej porażki.
- **Odwody:** postać jest wyznaczona do zadań bojowych, ale bardziej jako rezerwowy lub tylny strażnik (łucznicy wsparcia, artyleria). Tak naprawdę nie zobaczy walki, z wyjątkiem sytuacji, gdy wynik jest niekorzystny dla jego strony.
- **Zaangażowany:** postać wspiera pierwsze linie. Jest całkiem pewne, że zobaczy walkę, z wyjątkiem całkowitego zwycięstwa.
- **Pierwsza linia:** postać jest w środku najgorszej walki. Zawsze będzie miała swój udział w akcji.
- **Operacje specjalne** (lub "Specops"): postaci przydzielane jest zadanie za liniami wroga.
- 
Po pierwszej rundzie, każda postać może zmienić poziom zaangażowania, o **jeden** poziom. Za wyjątkiem operacji specjalnych. Aby przejść do poziomu "Specops", postać nie mógła być bardziej zaangażowana niż "wsparcie" w poprzedniej rundzie. 

## Rozstrzygnięcie rundy
Generał każdej armii przeciwnej będzie wykonywał rzut przeciwstawny na **Sztukę Wojny.** 
Do wyniku należy dodać następujące bonusy:

### Przewagi
|Warunki|Bonus|
|---|---|
| Broniący jest okopany |+5 do +20 w zależności od poziomu okopania|
|Przewaga liczebna|+1 za 10 % przewagi|
|Elitarne jednostki|+1 za 2% liczebności armii (maks +10)|
|Słabe jednostki (ashigaru,chłopi)|–1 za 5% liczebności armii|
|Lepsze wyposażenie (zbroje, sprzęt)|+5 do +20, do wiadomości MG|
|Warunki pogodowe|0 do +15 dla jednej ze stron, do wiadomości MG|
|Taktyczna przewaga z poprzedniej rundy|Zgodnie z akcjami bohaterów|
|Jakość łańcucha dowodzenia|–10 do +10, dw MG|
|Morale|–10 do +10, dw MG|
|Strategiczna przewaga z poprzedniej rundy|Różnica pomiędzy wynikiem zwycięzcy a przegranego z poprzedniej rundy (MS)|
|Efekt operacji specjalnych|wg MG|

### Wynik rundy
- Rzut przeciwstawny generałów na **Sztukę Wojny** i określenie różnicy rzutów oraz wyniku rundy w tabeli

|Różnica pomiędzy rzutami (MS)|Rezultat|
|---|---|
|0|Remis, żadna strona nie ma przewagi|
|1-5|Przewaga strategiczna, bez wyraźnego zwycięstwa|
|6-10|Małe zwycięstwo, zorganizowany odwrót przegranego|
|11-15|Duże zwycięstwo, przegrany w rozsypce|
|16+|Pełne zwycięstwo, przegrany zmiażdżony|

Jeśli taktyczne przewagi, zdobyte przez bohaterów strony przegrywającej są większe niż MS zwycięzcy, to obniżą jego MS  o połowę ich punktów przewagi taktycznej, zaokrągloną w dół.

- Rzut w tabeli zdarzeń dla każdego PC i ważnego NPC. **XK - Y** oznacza, że postać otrzymała **XK** obrażeń (pomniejszone o rzut na Wytrzymałość plus zbroja) oraz dodała **Y** punktów przewagi taktycznej swojemu generałowi. Gwiazdka przy Y oznacza, że postaci przydarzyło się coś specjalnego i może zdobyć więcej punktów przewagi taktycznej.

| |Rezultat| | | | | | |
|---|---|---|---|---|---|---|---|
| |Zwycięstwo|Wsparcie|Odwody|Zaangażowany|Pierwsza linia| | |
| |Remis| |Wsparcie|Odwody|Zaangażowany|Pierwsza linia| | 
| |Porażka| | |Wsparcie|Odwody|Zaangażowany|Pierwsza linia|
| |**2-4**|4K – 0|5K – 0|6K – 1|6K – 2|7K – 3*|8K – 4|
|Rzut K2 |**5-8**|4K – 0|4K – 1|5K – 1|6K – 2*|6K – 4|7K – 5|
|+Szt Wojny|**9-11**|3K – 1|4K – 1|4K – 2|5K – 3|6K – 4*|6K – 5*|
| |**12-13**|2K – 1|4K – 1*|4K – 2*|5K – 3*|5K – 5*|6K – 6*|
| |**14-16**|2K – 1*|3K – 1*|4K – 2|4K – 4*|5K – 5*|5K – 6*|
| |**17-19**|2K– 2|3K – 2*|3K – 3|4K – 4|4K – 6*|5K – 8*|
| |**20+**|1K – 2*|2K – 3|3K – 4|4K – 5|4K – 7*|4K – 10*|


