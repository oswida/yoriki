# Charakterystyki broni
@rules

## Strzały
| Rodzaj         | Rzut na obrażenia | Uwagi                 |
| -------------- | :---------------: | --------------------- |
| Armor Piercing |        1K         | Ignoruje bonus zbroi  |
| Flesh Cutter   |       3K+1        | Podwaja bonus zbroi   |
| Humming Bulb   |        1K         | Wydaje huczący dźwięk |
| Rope Cutter    |        1K         |                       |
| Willow Leaf    |       2K+1        |                       |

## Łuki
| Rodzaj         | Zasięg   | Bonus do obrażeń strzały | Uwagi |
| -------------- | -------- | :----------------------: | ----- |
| Dai-kyu - duży | 500 stóp |           1K+1           | -     |
| Han-kyu - mały | 100 stóp |            1             | -     |
| Yumi - duży    | 250 stóp |            1K            | -     |

## Łańcuchowe
| Rodzaj          | Obrażenia |         Opis         |   |
|-----------------|:---------:|:--------------------:|---|
| Kusarigama      |    2K     |   nóż na łańcuchu    |   |
| Kyouketsu-Shogi |    1K     |   hak na łańcuchu    |   |
| Manrikikusari   |    1K     | łancuch z ciężarkami |   |

## Ciężkie

| Rodzaj     | Obrażenia | Opis               |
|------------|:---------:|--------------------|
| Dai-Tsuchi |   2K+2    | Młot               |
| Masakari   |   3K+1    | jednoręczny topór  |
| Ono        |   4K-2    | dwuręczna siekiera |
| Tetsubo    |   3K+1    | pałka z ćwiekami   |

## Noże

| Rodzaj         | Obrażenia | Opis  |
|----------------|:---------:|-------|
| Aiguchi, Tanto |    1K     |       |
| Jitte, Sai     |    1K     |       |
| Kama           |   2K-1    | sierp |

## Ninjutsu

| Rodzaj   | Obrażenia | Opis |
|----------|:---------:|------|
| Blowgun  |     1     |      |
| Shuriken |    1K     |      |
| Tsubute  |    1K     |      |

## Drzewcowe

| Nazwa      | Obrażenia | Opis                                           |
| ---------- |:---------:| ---------------------------------------------- |
| Bisento    |   3K+1    | Ciężki miecz na bardzo długim kiju             |
| Nagamaki   |   3K-2    | Krótki kij z mieczem na końcu                  |
| Naginata   |   2K+1    | Średni kij, pom. Bisento a Nagamaki, z mieczem |
| Sasumata   |   2K-1    | do łapania ludzi                               |
| Sodegarami |    K1     | do przyszpilania                               |

## Włócznie

| Nazwa     | Obrażenia | Opis                |
| --------- |:---------:| ------------------- |
| Kumade    |    1K     | coś w rodzaju grabi |
| Mai-chong |   3K-2    | dziwny trójząb      |
| Lanca     |   4K+1    |                     |
| Nage-yari |    2K     | oszczep             |
| Yari      |   2K+1    | włócznia            |

## Kije

| Nazwa     | Obrażenia | Opis   |
|-----------|:---------:|--------|
| Bo        |    2K     | długi  |
| Jo        |   2K-1    | krótki |
| Nunchaku  |    2K     |        |
| Tonfa     |   3K-2    |        |

## Miecze

| Nazwa     | Obrażenia | Opis                |
| --------- |:---------:| ------------------- |
| Katana    |   2K+1    |                     |
| Ninja-to  |   2K+1    |                     |
| No-dachi  |   3K+1    | bardzo długa katana |
| Parangu   |   2K+1    | maczeta             |
| Scimitar  |   3K+1    | zakrzywiona szabla  |
| Wakizashi |   2K+1    |                     |

## Wachlarze

| Nazwa           | Obrażenia | Opis |
|-----------------|:---------:|------|
| Wachlarz bojowy |   1K-1    |      |
