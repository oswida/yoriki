# Podsumowanie reguł L5KD6
@rules

## Rozwój postaci
Rozwijamy jedynie umiejętności, atrybuty są elementami stałymi postaci. 

* Koszt jednego pipsa dla posiadanej umiejętności = numer kości dla tej umiejętności
* Jeden pips dla nowej umiejętności = numer kości dla jej atrybutu
* Jeden pips dla nowej specjalizacji = połowa kości atrybutu dla danej umiejętności
* Jeden pips rozwinięcia specjalizacji = połowa kości danej umiejętności

* W trakcie gry, postać może ewentualnie uzyskać Wady lub Zalety, koszty Zalet płacone są z PD (5 razy ilość kości w tabeli Zalet), Wady dają punkty do kupowania Zalet (5 razy ilość kości w tabeli Wad).
* Postać może także zwiększyć maksymalny poziom punktów Pustki – 10 razy aktualny poziom maksymalny ale poziom Pustki nie może przekroczyć kości Siły Woli.
* Istnieje możliwość zwiększenia kości atrybutu – wymaga to treningu określonego przez GM (część fabularna) i jeden pips kosztuje 5 razy aktualna kość atrybutu.

## Rzuty kością

* Każda pula rzutu zawiera jedną „dziką kość” (Wild Die), która jest przerzucana gdy wypadnie 6 lub daje efekty negatywne, gdy wypadnie 1
* Gracz może zużyć 1 PD (punkty doświadczenia) w dowolnym momencie (nawet po rzucie) ale przed określeniem rezultatu działania, aby dodać sobie jedną dodatkową dziką kość do puli (maksimum 3).
* Gracz może zużyć maksymalnie 1 punkt Pustki przed rzutem aby podwoić wszystkie ilości kości i pipsy dla danego rzutu (dzika kość pozostaje jedna).
* Podniesienie trudności (Raise/podbicie): w niektórych sytuacjach, gracz może dobrowolnie podnieść poziom o wielokrotność 4 (podbicie) aby uzyskać lepsze efekty działań

## Akcje

### Inicjatywa

* Rzut na Refleks, kolejność działań od najwyższego do najniższego wyniku
* W przypadku remisu, kolejność wg numeru kości przy Refleksie

### Rundy

* Postać nie musi deklarować akcji jaką podejmie zanim nie nadejdzie jej kolej
* Podczas swojej rundy postać może wykonać dowolną ilość akcji pod warunkiem stosowania kar za wiele akcji w trakcie tury. Każda dodatkowa akcja powyżej jednej dodaje karę -1K. Ilość przewidywanych akcji na rundę należy zadeklarować na początku ale nie trzeba określać co się będzie robiło.
* Postać może w swojej turze czekać wstrzymując się od działania – czekanie liczy się jako akcja
* Przykłady działań, które liczą się jako pojedyncza akcja w rundzie: uderzenie bronią/częścią ciała, złapanie obiektu, wyjaśnienie planów działania innej osobie, unik, rozbrojenie, rzut przedmiotem, skok, ruch z wykorzystaniem co najmniej 51% punktów ruchu postaci, dobycie broni, ucieczka, zablokowanie ataku, strzał, użycie umiejętności lub zdolności, czekanie.

### Poziomy sukcesu
Różnica pomiędzu rzutem a poziomem trudności

* Minimal ( 0): The total was just barely enough. The character hardly succeeded a tall, and only the most minimal effects apply. If "minimal effects" are not an option, then maybe the action took longer than normal to succeed.
* Solid (1-4): The action was performed completely, but without frills.
* Good (5-8): The results were better than necessary and there may be added benefits.
* Superior (9-12): There are almost certainly additional benefits to doing an action this well. The character performed the action better, faster, or more adeptly than expected.
* Spectacular (13-16): The character performed the action deftly and expertly. Observers would notice the ease or grace with which the action was performed (if applicable).
* Incredible (17 or more): The character performed the skill with such dazzling quality that, if appropriate to the task, it could become the subject of conversation for some time- it's atleast worth writing home about. Gamemasters should dole out some significant bonuses for getting this large of a roll.

## Walka
Określenie inicjatywy za pomocą rzutu na Refleks, akcje Ataku i Obrony wykonywane w rundach w kolejności inicjatywy:

### Atak
* Zastosuj modyfikatory do Ataku
* Rzut na Atak (zgodnie z użytą bronią lub techniką) i określenie trafienia poprzez porównanie z poziomem trudności Ataku wygenerowanym przez broniącego (brak obrony oznacza poziom trudności **10**). __Aktywna obrona możliwa jedynie przy odpowiedniej Inicjatywie__.
* Jeśli nastąpiło trafienie, rzut na obrażenia
* Zmniejszenie obrażeń wg ustalonego schematu (zbroja, rzut na Wytrzymałość, kość Wytrzymałości ) i określenie ran

### Obrona
* Dodaj modyfikatory do Obrony
* Rzut na Obronę – Uwaga! Rzut na obronę może być wykonany jedynie, jeśli postać jest odpowiednio szybka (Inicjatywa) i jest w stanie aktywnie się bronić, w przeciwnym wypadku Obrona wynosi **10**.
* Porównanie rzutu na Atak z poziomem trudności wygenerowanym przez rzut broniącego
* Jeśli nastąpiło trafienie, rzut na Wytrzymałość (plus modyfikatory zbroi) i odjęcie wyniku od rzutu na obrażenia wykonanego przez atakującego (opcjonalnie można używać tylko kości zbroi, bez Wytrzymałości – walka jest bardziej śmiertelna, lub połowa kości Wytrzymałości, lub tylko liczba kości Wytrzymałości)
* Zapisanie ran postaci

## Leczenie

* **Leczenie naturalne:** Rzut na Wytrzymałość (+1K za każdy pełny dzień odpoczynku, -1K za każdy dzień aktywny)
* **Leczenie za pomocą umiejętności Medycyna:** rzut na Medycynę raz dziennie per pacjent
* 
### Punkty Obrażeń

| Rzut  | Ilość odzyskanych punktów |
|-------|---------------------------|
| 0     | 0                         |
| 1-5   | 2                         |
| 6-10  | 1K                        |
| 11-15 | 2K                        |
| 16-20 | 3K                        |
| 21-25 | 4K                        |
| 26-30 | 5K                        |
| 31+ 0 | 6K                        |

**Naturalne**

* Ogłuszony: leczony po 12 rundach, ok 1 min odpoczynku
* Ranny, Ciężko ranny: 6+ redukuje ranę o jeden poziom; krytyczna porażka pogarsza o 1
* Nieprzytomny, śmiertelnie ranny: 8+ redukuje ranę o jeden poziom; ; krytyczna porażka pogarsza o 1 Umiejętność Pomyślny rzut redukuje o jeden poziom
* Ogłuszony – Łatwy (10), Ranny, Ciężko ranny – Średni (15), Nieprzytomny – Trudny (20), Śmiertelnie ranny – Bardzo trudny (25)

## Specjalne zdolności NPC
* Strach – patrz poniżej
* Niewrażliwość – postać może zostać zraniona jedynie zaklęciem, nemuranai, bronią z jadeitu, kryształu lub obsydianu
* Częściowa niewrażliwość: ataki podanego rodzaju zadają tylko 1 ranę
* Większa niewrażliwość – to samo co niewrażliwość ale dodatkowo odporność na jeden z ataków typu zaklęcia, jadeit etc.
* Odporność na magię
* Duch – tylko połowa obrażeń jeśli broń/zaklęcie nie zawiera jadeitu, kryształu lub obsydianu
* Nieumarły – brak kar za rany, niewrażliwość na efekty mentalne

## Pojedynki Iaijutsu

* Faza 1 **Oszacowanie**: Rzut na Iaijutsu PT = 10 + Honor przeciwnika, sukces pozwala uzyskać po jednej z poniższych informacji za każdy poziom sukcesu powyżej 0. 
  * Poziom Refleksu przeciwnika
  * Ilość punktów Pustki przeciwnika
  * Poziom umiejętności Iaijutsu przeciwnika
  * Aktualny poziom ran przeciwnika

Jeżeli uczestnik wyrzucił **10+** więcej niż jego przeciwnik otrzymuje dodatkowe 1K do rzutu w następnej fazie. Po zakończeniu tej fazy każdy z pojedynkujących może ustąpić uznając wyższość przeciwnika.

* Faza 2 **Skupienie:** Rzut na Iaijutsu i porównanie wyników pomiędzy pojedynkującymi. Jeżeli jeden z nich wyrzucił 5+ więcej od przeciwnika, otrzymuje prawo pierwszego ciosu, jeśli nie, obaj uderzają jednocześnie (kharmic strike)

* Faza 3 **Cios:** uczestnik, który ma prawo pierwszego ciosu wykonuje normalny atak za pomocą Iaijutsu, jeśli jego przeciwnik po tym ciosie jeszcze żyje, może wykonać swój atak. 
 
Jeżeli walka była prowadzona do pierwszej krwi a pierwszy atakujący zadał ranę, walka zostaje uznana za zakończoną i drugi przeciwnik przegrał (kontynuacja walki do pierwszej krwi oznacza utratę honoru). Jeżeli przeciwnicy wykonali atak jednoczesny, spór uznaje się za nierozstrzygnięty (brak zwycięzcy). Jeżeli jest to pojedynek na śmierć i życie, po wykonaniu pierwszych uderzeń, dalsza część prowadzona jest jak normalna walka z użyciem umiejętności walki na miecze (a nie Iaijutsu)

**Alternatywa:**
* Faza 1 **Oszacowanie:** Rzut na Iaijutsu PT = 10 + Honor przeciwnika, sukces pozwala uzyskać po jednej z poniższych informacji za każdy poziom sukcesu powyżej 0. 
  * Poziom Siły Woli przeciwnika
  * Ilość punktów Pustki przeciwnika
  * Poziom umiejętności Iaijutsu przeciwnika
  * Aktualny poziom ran przeciwnika

Jeżeli uczestnik  w fazie Oszacowania wyrzucił 10+ więcej niż jego przeciwnik otrzymuje prawo decyzji, kto  zaczyna deklarację w następnej fazie.
* Faza 2 **Skupienie lub Cios** W tej fazie każdy z przeciwników wykonuje naprzemiennie rundę deklarując „Skupienie” lub „Cios”. Jeżeli w poprzedniej fazie nie było znaczącego zwycięzcy, obaj szermierze wykonują rzut na Siłę Woli, wygrywający decyduje o kolejności licytacji.
  * *Skupienie:* podnosi PT własnego Ciosu. Podstawowy PT=5.  Każde  skupienie podnosi trudność o +5. Postać może wykonać tylko tyle „skupień” ile wynosi jego kość Siły Woli. 
  * *Cios:*  szermierz przerywa licytację i pozwala przeciwnikowi wykonać pierwszy cios (za pomocą Iaijutsu) z jego (przeciwnika) poziomem trudności.  Jeśli po tym ciosie jeszcze żyje, może wykonać swój atak, ze swoim własnym poziomem trudności.

Jeżeli walka była prowadzona do pierwszej krwi a pierwszy atakujący zadał ranę, walka zostaje uznana za zakończoną i drugi przeciwnik przegrał (kontynuacja walki do pierwszej krwi oznacza utratę honoru)
Jeżeli jest to pojedynek na śmierć i życie, po wykonaniu pierwszych uderzeń, dalsza część prowadzona jest jak normalna walka z użyciem umiejętności walki na miecze (a nie Iaijutsu)


## Umiejętności „medytacja” i „ceremonia herbaciana”
Umiejętności te pozwalają na odzyskiwanie punktów Pustki ale tylko do poziomu maksymalnego i to raz dziennie. 

* **Medytacja:** Rzut na umiejętność PT=13 pozwala na odzyskanie jednego punktu Pustki na pół godziny nieprzerwanej medytacji.
* **Ceremonia herbaciana:** Rzut na umiejętność PT=11 plus 2 pkt za każdego uczestnika powyżej dwóch. Po pół godziny, każdy uczestnik odzyskuje 1 punkt Pustki. Ceremonia wymaga spokojnego i cichego miejsca.

## Strach

* Rzut Siła Woli + Modyfikator z Honoru (liczba dużych punktów Honoru dzielona przez 3 zaokrąglona do dołu) przeciwko PT = 5 + 5*Poziom Strachu, porażka oznacza modyfikator -X do wszystkich rzutów, gdzie X = Poziom Strachu  

## Honor, Chwała i Status

### Honor
Honor primarily reflects how an individual samurai rates his personal ability to adhere to the tenets of Bushido and fulfill the duties assigned to him by his lord. While Honor is primarily internal, there is an external component to it as well, as a samurai’s Honor greatly impacts how he carries himself and thus how others perceive him. Honor Ranks can be roughly described as follows:

* 0-1: Pies bez honoru
* 2-3: Niegodny zaufania
* 4-5: Standard
* 6-7: Wyjątkowy
* 8-9: Dusza ponad wszelkie wątpliwości
* 10: Siła tysiąca przodków

**Efekt Honoru:** Dodatkowe X kości do rzutów przeciwko: Kuszeniu, Zastraszaniu i Strachowi, gdzie X to liczba dużych punktów Honoru dzielona przez 3 i zaokrąglana do dołu.

### Chwała

**Efekt Chwały:** Udany rzut na Wiedza: Heraldyka PT=30 – Chwała*3 powoduje, że postać jest rozpoznawana przez obcych ludzi i traktowana z odpowiednim szacunkiem.

### Status

**Efekt Statusu:** Każda postać o statusie niższym musi być posłuszna tej o wyższym.


## Skaza Cienia

**Zarażenie/zwiększenie** – za każdym razem gdy postać jest narażona na kontakt ze Skazą, musi wykonać rzut na Siłę Woli/Wytrzymałość (co jest mniejsze), każda porażka daje 1 mały punkt Skazy. 10 punktów Skazy daje jedną Rangę. Poziomy trudności w zależności od sytuacji:
* Podróżowanie przez Krainy Cienia bez jadeitu (dziennie): 6
* Ranny w Krainach Cienia: 6
* Oblany/opryskany skażoną krwią lub innym płynem:  6
* Fizyczna bliskość/kontakt z silnie skażoną postacią: 6
* Użycie pomniejszej mocy Krain Cienia:  11
* Zraniony (ugryzienie/użądlenie/rany od pazurów) przez skażoną postać: 11
* Ugodzony przez skażoną broń: 11
* Noszenie znaku Oni (na tydzień): 11
* Zanurzony w skażonym płynie (wodzie lub innym): 11
* Użycie większej mocy Krain Cienia: 13
* Zjedzenie/wypicie skażonego jedzenia/wody: 13
* Połknięcie żywcem przez Oni: 21

**Wzrost** – okresowy rzut na Siłę Woli/Wytrzymałość (co jest mniejsze), każda porażka daje 1 mały punkt Skazy. 
* Ziarna ciemności, raz na miesiąc, PT=1
* Poziom 1, raz na miesiąc, PT=6
* Poziom 2, raz na 2 tygodnie, PT=11
* Poziom 3, raz na tydzień, PT=13
* Poziom 4, raz na dzień, PT=16
* 
**Ochrona**
Każdy, kto regularnie pije herbatę Jadeitowych Płatków dostaje bonus 2K-2 do rzutów
Każdy, kto ma przy sobie jadeit otrzymuje +6 do rzutów na odporność

**Poziomy infekcji**

* Ziarna Ciemności – sporadycznie koszmary nocne
* Poziom 1: koszmary nocne, jadeit parzy skórę, zaklęcia oparte na jadeicie wywierają efekt na postaci
* Poziom 2: dolegliwości cielesne - wymioty, zawroty głowy, brzydki zapach ciała, jedna pomniejsza moc Krain Cienia
* Poziom 3: napady złości, paranoje, dodatkowa pomniejsza moc Krain Cienia plus jedna mutacja, -3 do wszystkich rzutów socjalnych
* Poziom 4: przy każdej stresującej sytuacji rzut na Siłę Woli PT=11 albo postać wpada w szał, -6 do rzutów socjalnych, -2K-2 do rzutów na Honor, jedna dodatkowa pomniejsza i jedna większa moc Krain Cienia, jedna dodatkowa mutacja, maksymalna ilość punktów Pustki zredukowana o 1
* Poziom 5: Zagubiony – całkowicie pochłonięty przez Skazę.

## Magia

* Rzut na umiejętność Magia z odpowiedniego kręgu przeciwko PT = 5 + (5*Poziom zaklęcia/Mastery) dopasowane do D6

* Ilość wykorzystanych czarów danego rodzaju nie może przekroczyć kości magii danego rodzaju  plus dodatkowe czary za maksymalny poziom Pustki. Nieudany rzut nadal zużywa slot zaklęcia