# Leczenie
@rules

- *Naturalne:* Po odczekaniu okresu z poniższej tabeli, postać rzuca na **Wytrzymałość** (+1K za każdy pełny dzień odpoczynku poprzedzający rzut (również te, które trzeba było czekać), -1K za każdy poprzedzający dzień aktywny). PO powracają rankiem następnego dnia. 
- *Medycyna:* Rzut na Medycynę raz dziennie tylko jeden pacjent. W systemie Wounds, leczony jest jeden poziom ran. 

## Leczenie w systemie Body Points

|    Poziom ran     | Czas oczekiwania |
| :---------------: | :--------------: |
|     Ogłuszony     |     1 minuta     |
|       Ranny       |      3 dni       |
|   Ciężko ranny    |      3 dni       |
|   Nieprzytomny    |    2 tygodnie    |
| Śmiertelnie ranny |    5 tygodni     |

---

| Wynik rzutu | Wyleczone pkty |
| :---------: | :------------: |
|      0      |       0        |
|     1-5     |       2        |
|    6-10     |       1K       |
|    11-15    |       2K       |
|    16-20    |       3K       |
|    21-25    |       4K       |
|    26-30    |       5K       |
|     31+     |       6K       |

## Leczenie w systemie Wounds

|    Poziom ran     |                         Rzuty i efekty                          |
| :---------------: | :-------------------------------------------------------------: |
|     Ogłuszony     |          Automatycznie leczony po 1 minucie odpoczynku          |
|       Ranny       | 6+ redukuje ranę o jeden poziom; krytyczna porażka pogarsza o 1 |
|  Poważnie ranny   | 6+ redukuje ranę o jeden poziom; krytyczna porażka pogarsza o 1 |
|   Nieprzytomny    | 8+ redukuje ranę o jeden poziom; krytyczna porażka pogarsza o 1 |
| Śmiertelnie ranny | 8+ redukuje ranę o jeden poziom; krytyczna porażka pogarsza o 1 |

---

|    Poziom ran     | Poziom trudności rzutu na Medycynę |
| :---------------: | :--------------------------------: |
|     Ogłuszony     |              Łatwy 6               |
|       Ranny       |             Średni 11              |
|  Poważnie ranny   |             Średni 11              |
|   Nieprzytomny    |             Trudny 13              |
| Śmiertelnie ranny |          Bardzo trudny 16          |