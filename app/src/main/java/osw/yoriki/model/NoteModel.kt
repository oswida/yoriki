package osw.yoriki.model

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.yoriki.R
import osw.yoriki.db.Note
import osw.yoriki.db.DbWorker
import java.util.*

class NoteHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var title: TextView? = null
    var tags: TextView? = null

    init {
        title = itemView.findViewById(R.id.title)
        tags = itemView.findViewById(R.id.tags)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }

    override fun getRearLeftView(): View {
        return itemView.findViewById(R.id.rear_left_view)
    }

    override fun getRearRightView(): View {
        return itemView.findViewById(R.id.rear_right_view)
    }
}

class NoteItem(val item: Note) : AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as NoteHolder).title?.text = item.title
        (holder as NoteHolder).tags?.text = item.tags
        // TODO; fill level
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): NoteHolder {
        return NoteHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is NoteItem) {
            val inItem = other
            return hashCode() == inItem.hashCode()
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(item.id!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.note_item
    }

    override fun toString(): String {
        return item.title!!
    }

}

class NoteModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<NoteItem>> = MutableLiveData()
    var filter = ""

    fun refresh(filter: String = "") {
        DbWorker.postDbTask(app.applicationContext) { db ->
            var list: List<NoteItem>
            if (filter.isEmpty()) {
                list = db.noteDao().getAll().sortedBy { it.title }.map { NoteItem(it) }
            } else {
                if (filter.startsWith("@")) {
                    list = db.noteDao().getAllWithTag("%$filter%").sortedBy { it.title }.map { NoteItem(it) }
                } else {
                    list = db.noteDao().getAll("$filter%").sortedBy { it.title }.map { NoteItem(it) }
                }
            }
            itemList.postValue(list)
        }
    }
}