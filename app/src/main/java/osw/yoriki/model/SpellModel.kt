package osw.yoriki.model

import android.app.Application
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.squareup.moshi.Moshi
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.yoriki.R
import osw.yoriki.db.RpgSpell
import osw.yoriki.util.FileUtils
import osw.yoriki.util.TextUtils
import java.util.*

class SpellHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var title: TextView? = null
    var level: TextView? = null
    var icon: ImageView? = null


    init {
        title = itemView.findViewById(R.id.title)
        level = itemView.findViewById(R.id.level)
        icon = itemView.findViewById(R.id.spelltype)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }
}

class SpellItem(val title: String, val path: String) : AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as SpellHolder).title?.text = title
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<RpgSpell>(RpgSpell::class.java)
        val spell = jsonAdapter.fromJson(
            FileUtils.textFromAsset(holder.frontView.context!!.assets, path)
        )!!
        holder.level!!.text = spell.mastery.toString()
        if (spell.keywords.contains("Maho")) {
            holder.icon!!.setImageResource(R.drawable.ic_symbol_maho)
        } else if (spell.ring.contains("Air")) {
            holder.icon!!.setImageResource(R.drawable.ic_symbol_air)
        } else if (spell.ring.contains("Water")) {
            holder.icon!!.setImageResource(R.drawable.ic_symbol_water)
        } else if (spell.ring.contains("Fire")) {
            holder.icon!!.setImageResource(R.drawable.ic_symbol_fire)
        } else if (spell.ring.contains("Earth")) {
            holder.icon!!.setImageResource(R.drawable.ic_symbol_earth)
        } else if (spell.ring.contains("Void")) {
            holder.icon!!.setImageResource(R.drawable.ic_symbol_void)
        }

    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): SpellHolder {
        return SpellHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is SpellItem) {
            val inItem = other
            return hashCode() == inItem.hashCode()
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(title)
    }

    override fun getLayoutRes(): Int {
        return R.layout.spell_item
    }

    override fun toString(): String {
        return title
    }

}

class SpellModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<SpellItem>> = MutableLiveData()

    fun refresh(filter: String = "") {
        val list = app.assets.list("spells")
        itemList.postValue(list
            .filter {
                filter.isEmpty() || it.toLowerCase().startsWith(filter.toLowerCase())
            }
            .sorted()
            .map {
                SpellItem(
                    TextUtils.removeJsonExts(it), "spells/$it"
                )
            })
    }
}