package osw.yoriki.model

import android.app.Application
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.yoriki.R
import osw.yoriki.db.DChar
import osw.yoriki.db.DbWorker
import osw.yoriki.fragment.CharlistFragment
import java.util.*

class DCharHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var name: TextView? = null

    var noteBtn: ImageView? = null
    var icon: ImageView? = null

    init {
        name = itemView.findViewById(R.id.name)
        noteBtn = itemView.findViewById(R.id.noteBtn)
        icon = itemView.findViewById(R.id.icon)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }

    override fun getRearLeftView(): View {
        return itemView.findViewById(R.id.rear_left_view)
    }

    override fun getRearRightView(): View {
        return itemView.findViewById(R.id.rear_right_view)
    }
}

class DCharItem(val item: DChar, val fragment: CharlistFragment) : AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as DCharHolder).name?.text = item.name
        holder.noteBtn!!.setOnClickListener {
            fragment.showNotes(item)
        }
        when (item.type) {
            "PC" -> holder.icon!!.setImageResource(R.drawable.ic_pc)
            "NPC" -> holder.icon!!.setImageResource(R.drawable.ic_npc)
            "Creature" -> holder.icon!!.setImageResource(R.drawable.ic_creature)
        }
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): DCharHolder {
        return DCharHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is DCharItem) {
            val inItem = other
            return hashCode() == inItem.hashCode()
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(item.name!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.dchar_item
    }

    override fun toString(): String {
        return item.name!!
    }

}

class DCharModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<DCharItem>> = MutableLiveData()
    var filter = ""

    fun refresh(filter: String = "", fragment:CharlistFragment) {
        DbWorker.postDbTask(app.applicationContext) { db ->
            var list: List<DCharItem>
            if (filter.isEmpty()) {
                list = db.dcharDao().getAll().sortedBy { it.name }.map { DCharItem(it, fragment) }
            } else {
                list = db.dcharDao().getAll("$filter%").sortedBy { it.name }.map { DCharItem(it, fragment) }
            }
            itemList.postValue(list)
        }
    }
}