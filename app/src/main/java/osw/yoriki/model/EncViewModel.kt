package osw.yoriki.model

import android.app.Application
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.yoriki.R
import osw.yoriki.db.DbWorker
import osw.yoriki.db.RpgParticipant
import osw.yoriki.fragment.EncViewFragment
import osw.yoriki.util.MiscUtils
import osw.yoriki.util.TextUtils
import osw.yoriki.util.spacePadded
import java.util.*

class EncViewHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var name: TextView? = null
    var bp: Button? = null
    var battle: TextView? = null
    var inv: TextView? = null
    var actionMod: Button? = null
    var status: TextView? = null
    var actions: TextView? = null
    var attMod: Button? = null
    var defMod: Button? = null

    var statusIcon: ImageView? = null
    var actionsIcon: ImageView? = null
    var activeBtn: ImageView? = null

    init {
        name = itemView.findViewById(R.id.name)
        bp = itemView.findViewById(R.id.bp)
        battle = itemView.findViewById(R.id.battle)
        inv = itemView.findViewById(R.id.inv)
        actionMod = itemView.findViewById(R.id.actionMod)
        status = itemView.findViewById(R.id.status)
        statusIcon = itemView.findViewById(R.id.statusIcon)
        actions = itemView.findViewById(R.id.actions)
        actionsIcon = itemView.findViewById(R.id.actionsIcon)
        attMod = itemView.findViewById(R.id.attMod)
        defMod = itemView.findViewById(R.id.defMod)
        activeBtn = itemView.findViewById(R.id.activeBtn)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }

    override fun getRearLeftView(): View {
        return itemView.findViewById(R.id.rear_left_view)
    }

    override fun getRearRightView(): View {
        return itemView.findViewById(R.id.rear_right_view)
    }
}

class EncViewItem(
    val item: RpgParticipant,
    val fragment: EncViewFragment
) : AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as EncViewHolder).name?.text = item.stats.name
        holder.bp?.text = item.bp.toString().spacePadded()
        holder.bp?.setOnClickListener {
            fragment.setBodyPoints(item)
        }
        holder.battle?.text = item.stats.battleStats(true)
        holder.inv?.text = "Inicjatywa:" + item.initiative.toString().spacePadded()

        holder.status?.text = item.state
        if (item.state.isBlank()) {
            holder?.statusIcon!!.visibility = ViewGroup.GONE
        } else {
            holder?.statusIcon!!.visibility = ViewGroup.VISIBLE
        }
        holder.actions?.text = item.actions.joinToString(",")
        if (item.actions.isEmpty()) {
            holder?.actionsIcon!!.visibility = ViewGroup.GONE
        } else {
            holder?.actionsIcon!!.visibility = ViewGroup.VISIBLE
        }
        holder.actionMod!!.setOnClickListener {
            fragment.setActionModifier(item)
        }
        val dm = item.actMod + item.stats.woundModifier(item.bp)
        holder.actionMod!!.text = dm.toString().spacePadded()

        holder.attMod!!.setOnClickListener {
            fragment.setAttModifier(item)
        }
        holder.attMod!!.text = item.attMod.toString().spacePadded()

        holder.defMod!!.setOnClickListener {
            fragment.setDefModifier(item)
        }
        holder.defMod!!.text = item.defMod.toString().spacePadded()
        if (fragment.activeParticipant != null && fragment.activeParticipant!!.id == item.id) {
            holder.activeBtn!!.visibility = ViewGroup.VISIBLE
        } else {
            holder.activeBtn!!.visibility = ViewGroup.GONE
        }
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): EncViewHolder {
        return EncViewHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is EncViewItem) {
            val inItem = other
            return hashCode() == inItem.hashCode()
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(item.stats.name!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.encview_item
    }

    override fun toString(): String {
        return item.stats.name!!
    }

}

class EncViewModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<EncViewItem>> = MutableLiveData()

    fun refresh(chars: List<RpgParticipant>, fragment: EncViewFragment) {
        DbWorker.postDbTask(app.applicationContext) { db ->
            var list = chars.sortedBy { it.initiative }
                .reversed().map { EncViewItem(it, fragment) }
            itemList.postValue(list)
        }
    }
}