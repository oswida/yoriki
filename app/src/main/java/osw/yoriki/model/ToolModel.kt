package osw.yoriki.model

import android.app.Application
import android.view.View
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.yoriki.R
import java.util.*

class ToolHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var title: TextView? = null

    init {
        title = itemView.findViewById(R.id.title)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }

}

class ToolItem(val title: String) : AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as ToolHolder).title?.text = title
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): ToolHolder {
        return ToolHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is ToolItem) {
            val inItem = other
            return hashCode() == inItem.hashCode()
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(title)
    }

    override fun getLayoutRes(): Int {
        return R.layout.tool_item
    }

    override fun toString(): String {
        return title
    }

}

class ToolModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<ToolItem>> = MutableLiveData()
    var tools = listOf(
        app.applicationContext.getString(R.string.CoopCalcTool),
        app.applicationContext.getString(R.string.SpellDiffTool),
        app.applicationContext.getString(R.string.L5KLevelConverter),
        app.applicationContext.getString(R.string.L5KRollConverter),
        app.applicationContext.getString(R.string.L5KCharConverter)
    )
    var filter = ""

    fun refresh(filter: String = "") {
        itemList.postValue(tools
            .filter { filter.isBlank() || it.toLowerCase().startsWith(filter.toLowerCase()) }
            .map { ToolItem(it) })
    }
}