package osw.yoriki.model

import android.app.Application
import android.view.View
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.yoriki.R
import osw.yoriki.util.TextUtils
import java.util.*

class MapHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var title: TextView? = null

    init {
        title = itemView.findViewById(R.id.title)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }

//    override fun getRearLeftView(): View {
//        return itemView.findViewById(R.id.rear_left_view)
//    }
//
//    override fun getRearRightView(): View {
//        return itemView.findViewById(R.id.rear_right_view)
//    }
}

class MapItem(val title: String, val path: String) : AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as MapHolder).title?.text = title
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): MapHolder {
        return MapHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is MapItem) {
            val inItem = other
            return hashCode() == inItem.hashCode()
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(title)
    }

    override fun getLayoutRes(): Int {
        return R.layout.map_item
    }

    override fun toString(): String {
        return title
    }

}

class MapModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<MapItem>> = MutableLiveData()

    fun refresh(filter: String = "") {
        val list = app.assets.list("maps")
        itemList.postValue(list
            .sorted()
            .map {
                MapItem(
                    TextUtils.removeImageExts(it), "maps/$it"
                )
            })
    }
}