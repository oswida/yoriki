package osw.yoriki.model

import android.app.Application
import android.view.View
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.squareup.moshi.Moshi
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import osw.yoriki.R
import osw.yoriki.db.Encounter
import osw.yoriki.db.DbWorker
import osw.yoriki.db.RpgEncounter
import java.util.*

class EncounterHolder(
    itemView: View,
    adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
) :
    FlexibleViewHolder(itemView, adapter) {

    var title: TextView? = null
    var num: TextView? = null

    init {
        title = itemView.findViewById(R.id.title)
        num = itemView.findViewById(R.id.num)
    }

    override fun getFrontView(): View {
        return itemView.findViewById(R.id.front_view)
    }

    override fun getRearLeftView(): View {
        return itemView.findViewById(R.id.rear_left_view)
    }

    override fun getRearRightView(): View {
        return itemView.findViewById(R.id.rear_right_view)
    }
}

class EncounterItem(val item: Encounter) : AbstractFlexibleItem<FlexibleViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FlexibleViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        (holder as EncounterHolder).title?.text = item.title
        val moshi = Moshi.Builder().build()
        val adapter = moshi.adapter(RpgEncounter::class.java)
        val enc = adapter.fromJson(item.data)
        (holder as EncounterHolder).num?.text = enc!!.chars.size.toString()
    }

    override fun createViewHolder(
        view: View?,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ): EncounterHolder {
        return EncounterHolder(view!!, adapter!!)
    }

    override fun equals(other: Any?): Boolean {
        if (other is EncounterItem) {
            val inItem = other
            return hashCode() == inItem.hashCode()
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(item.title!!)
    }

    override fun getLayoutRes(): Int {
        return R.layout.encounter_item
    }

    override fun toString(): String {
        return item.title!!
    }

}

class EncounterModel(val app: Application) : AndroidViewModel(app) {

    var itemList: MutableLiveData<List<EncounterItem>> = MutableLiveData()
    var filter = ""

    fun refresh(filter: String = "") {
        DbWorker.postDbTask(app.applicationContext) { db ->
            var list: List<EncounterItem>
            if (filter.isEmpty()) {
                list = db.encounterDao().getAll().sortedBy { it.title }.map { EncounterItem(it) }
            } else {
                list = db.encounterDao().getAll("$filter%").sortedBy { it.title }.map { EncounterItem(it) }
            }
            itemList.postValue(list)
        }
    }
}