package osw.yoriki.db

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RpgParticipant(
    var id: Long,
    var defMod: Int,
    var attMod: Int,
    var dmgMod: Int,
    var actMod: Int,
    var bp: Int,
    var state: String,
    var actions: List<String>,
    var stats: RpgCharacter,
    var initiative: Int = 0
)

@JsonClass(generateAdapter = true)
data class RpgEncounter(
    var defMod: Int,
    var attMod: Int,
    var dmgMod: Int,
    var actMod: Int,
    var description: String,
    var chars: List<RpgParticipant>
)