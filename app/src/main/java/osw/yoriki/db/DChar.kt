package osw.yoriki.db

import androidx.room.*

@Entity(tableName = "dchar")
data class DChar(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "name") var name: String?,
    @ColumnInfo(name = "type") var type: String?,
    @ColumnInfo(name = "data") var data: String?
)

@Dao
interface DCharDao {

    @Query("SELECT * FROM dchar")
    fun getAll(): List<DChar>

    @Query("SELECT * FROM dchar WHERE name LIKE :filter")
    fun getAll(filter: String): List<DChar>

    @Query("SELECT * FROM dchar WHERE id=:id")
    fun findById(id: Long): DChar?

    @Insert
    fun insertAll(vararg items: DChar)

    @Delete
    fun delete(item: DChar)

    @Query("DELETE FROM dchar WHERE id=:id")
    fun deleteById(id: Long)

    @Query("DELETE FROM dchar")
    fun clear()

    @Update
    fun save(item: DChar)
}