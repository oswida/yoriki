package osw.yoriki.db

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RpgSpell(
    val name: String,
    val ring: String,
    val mastery: Int,
    val range: String,
    val area: String,
    val duration: String,
    val description: String,
    val keywords: String = ""
) {

    fun toMarkdown(): String {
        return "# ${name}\n **Ring:** ${ring}    \n**Mastery:** ${mastery}    \n**Keywords:** ${keywords}    \n**Range:** ${range}    \n**Area:** ${area}    \n**Duration:** ${duration}    \n**Description:** ${description}    \n"
    }
}