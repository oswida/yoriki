package osw.yoriki.db

import androidx.room.*

@Entity(tableName = "encounter")
data class Encounter(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "data") var data: String?
)

@Dao
interface EncounterDao {

    @Query("SELECT * FROM encounter")
    fun getAll(): List<Encounter>

    @Query("SELECT * FROM encounter WHERE title LIKE :filter")
    fun getAll(filter: String): List<Encounter>

    @Query("SELECT * FROM encounter WHERE id=:id")
    fun findById(id: Long): Encounter?

    @Query("SELECT * FROM encounter WHERE title=:title")
    fun findByTitle(title: String): Encounter?

    @Insert
    fun insertAll(vararg items: Encounter)

    @Delete
    fun delete(item: Encounter)

    @Query("DELETE FROM encounter WHERE id=:id")
    fun deleteById(id: Long)

    @Query("DELETE FROM encounter")
    fun clear()

    @Update
    fun save(item: Encounter)
}