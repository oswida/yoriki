package osw.yoriki.db

import androidx.room.*

@Entity(tableName = "note")
data class Note(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "tags") var tags: String?,
    @ColumnInfo(name = "content") var content: String?
)

@Dao
interface NoteDao {

    @Query("SELECT * FROM note")
    fun getAll(): List<Note>

    @Query("SELECT * FROM note WHERE title LIKE :filter")
    fun getAll(filter: String): List<Note>

    @Query("SELECT * FROM note WHERE tags LIKE :filter")
    fun getAllWithTag(filter: String): List<Note>

    @Query("SELECT * FROM note WHERE id=:id")
    fun findById(id: Long): Note?

    @Query("SELECT * FROM note WHERE title=:title")
    fun findByTitle(title: String): Note?

    @Insert
    fun insertAll(vararg items: Note)

    @Delete
    fun delete(item: Note)

    @Query("DELETE FROM note WHERE id=:id")
    fun deleteById(id: Long)

    @Query("DELETE FROM note")
    fun clear()

    @Update
    fun save(item: Note)
}