package osw.yoriki.db

import com.squareup.moshi.JsonClass
import osw.yoriki.util.CnvUtils
import osw.yoriki.util.TextUtils
import kotlin.math.ceil

@JsonClass(generateAdapter = true)
data class RpgSkill(
    var name: String,
    var roll: String
)

@JsonClass(generateAdapter = true)
data class RpgCharacter(
    var name: String,
    var type: String,
    var initiative: String,
    var attack: String,
    var damage: String,
    var bp: Int,
    var stamina: String,
    var willpower: String,
    var perception: String,
    var strength: String,
    var reflexes: String,
    var awareness: String,
    var agility: String,
    var intelligence: String,
    var magic: String,
    var honor: Float = 0.0f,
    var glory: Float = 0.0f,
    var status: Float = 0.0f,
    var fear: Int = 0,
    var taint: Float = 0.0f,
    var skills: MutableList<RpgSkill> = mutableListOf(),
    var extras: String = "",
    var equipment: String = "",
    var description: String = "",
    var notes: String = "",
    var voidpts: Int = 0,
    var spells: List<String> = listOf()
) {

    fun trans(text: String, on: Boolean): String {
        if (on) {
            var result = CnvUtils.attrtrans[text]
            if (result != null) {
                return result
            }
            result = CnvUtils.fighttrans[text]
            if (result != null) {
                return result
            }
            result = CnvUtils.soctrans[text]
            if (result != null) {
                return result
            }
            result = CnvUtils.shadowtrans[text]
            if (result != null) {
                return result
            }
            result = CnvUtils.skilltrans[text]
            if (result != null) {
                return result
            }
            result = CnvUtils.misctrans[text]
            if (result != null) {
                return result
            }
        }
        return text
    }

    fun toSinglePageMarkdown(translate: Boolean = false): String {
        var text = "# $name \n\n --${type}--\n\n"
        text += TextUtils.mdTableStart("", "", "", "", "", "")
        text += TextUtils.mdTableRow(
            trans("Initiative", translate),
            "**${initiative}**",
            trans("Attack", translate),
            "**$attack**"
        )
        text += TextUtils.mdTableRow(
            trans("Damage", translate),
            "**$damage**",
            trans("Body Points", translate),
            "**$bp**"
        )
        text += TextUtils.mdTableRow(
            trans("Fear", translate),
            "**$fear**",
            trans("Taint", translate),
            "**$taint**"
        )
        text += TextUtils.mdTableRow(
            trans("Honor", translate),
            "**$honor**",
            trans("Glory", translate),
            "**$glory**"
        )
        text += TextUtils.mdTableRow(
            trans("Status", translate),
            "**$status**",
            trans("Void Points", translate),
            "**$voidpts**"
        )
        text += "\n\n### ${trans("Attributes",translate)}\n"
        text += TextUtils.mdTableStart("", "", "", "")
        text += TextUtils.mdTableRow(
            trans("Stamina", translate),
            "**$stamina**",
            trans("Willpower", translate),
            "**$willpower**"
        )
        text += TextUtils.mdTableRow(
            trans("Perception", translate),
            "**$perception**",
            trans("Strength", translate),
            "**$strength**"
        )
        text += TextUtils.mdTableRow(
            trans("Reflexes", translate),
            "**$reflexes**",
            trans("Awareness", translate),
            "**$awareness**"
        )
        text += TextUtils.mdTableRow(
            trans("Agility", translate),
            "**$agility**",
            trans("Intelligence", translate),
            "**$intelligence**"
        )
        text += TextUtils.mdTableRow(trans("Magic", translate), "**$magic**", " ", " ")
        if (skills.isNotEmpty()) {
            text += "\n\n### ${trans("Skills", translate)}    \n"
            text += skills.map { "${trans(it.name, translate)}: **${it.roll}**" }.joinToString(", ")
            text += "\n\n"
        }
        text += description + "\n\n"
        if (extras.isNotBlank()) {
            text += "\n\n### ${trans("Extras", translate)}     \n"
            text += extras
        }
        if (spells.isNotEmpty()) {
            text += "\n\n### ${trans("Spells", translate)}     \n"
            text += spells.joinToString(",")
        }
        if (equipment.isNotBlank()) {
            text += "\n\n### ${trans("Equipment", translate)}    \n"
            text += equipment
        }
        if (notes.isNotBlank()) {
            text += "\n\n### ${trans("Notes", translate)}    \n"
            text += notes
        }
        return text
    }

    fun battleStats(translate: Boolean): String {
        var text = ""
        if (type != "PC") {
            text = trans("Initiative", translate) + ": $initiative   " +
                    trans("Attack", translate) + ": $attack   "+
                    trans("Damage", translate) + ": $damage   "
        } else {
            skills.forEach {
                if (CnvUtils.battleSkills.contains(it.name)) {
                    val nm = trans(it.name, translate)
                    text += "$nm: ${it.roll}   "
                }
            }
        }
        text += trans("Stamina", translate) + ": $stamina   "
        text += trans("Strength Dmg", translate) + ": "+strengthDamage()+"   "
        return text
    }

    fun woundModifier(current: Int): Int {
        if (bp <= 0) {
            return 0
        }
        val pcnt = ((current.toFloat()/bp.toFloat()) * 100.0f).toInt()
        if (pcnt in 40..59) {
            return -1
        } else if (pcnt in 20..39) {
            return -2
        } else if (pcnt in 10..19) {
            return -3
        }  else if (pcnt in 1..9) {
            return -5
        } else if (pcnt <= 0) {
            return -100
        }
        return 0
    }

    fun strengthDamage():Int {
        val dice = CnvUtils.diceFromString(stamina)
        return  ceil(dice.toFloat()/2.0f).toInt()
    }

}