package osw.yoriki.fragment

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.squareup.moshi.Moshi
import com.thejuki.kformmaster.helper.*
import com.thejuki.kformmaster.model.BaseFormElement
import com.thejuki.kformmaster.model.FormHeader
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.DChar
import osw.yoriki.db.DbWorker
import osw.yoriki.db.RpgCharacter
import osw.yoriki.db.RpgSkill
import osw.yoriki.util.*

@RouterPath(name = "charedit")
class CharEditFragment : Fragment() {

    private val HEADER_TAG = 200000
    private val EQUIP_TAG = 200001
    private val DESC_TAG = 200002
    private val NOTES_TAG = 200003
    private val EXTRAS_TAG = 200004
    private val NAME_TAG = 200005
    private val TYPE_TAG = 200006
    private val ATTR_TAGS = 100
    private val FIGHT_TAGS = 200
    private val SOC_TAGS = 300
    private val SHADOW_TAGS = 400
    private val SKILL_TAGS = 500
    private val VOIDPTS_TAG = 200007
    private val SPELLS_TAG = 200008


    @Argument
    var id: Long = -1L

    private var recyclerView: RecyclerView? = null
    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private var formBuilder: FormBuildHelper? = null
    private val moshi = Moshi.Builder().build()
    private var filter = ""
    private val skillList = mutableListOf<String>()
    private var spellList = listOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.note_edit, container, false)
        recyclerView = result.findViewById(R.id.formview)
        spellList = context!!.assets.list("spells").map { TextUtils.removeJsonExts(it) }
        initForm()
        return result
    }

    fun initForm() {
        formBuilder = form(context!!, recyclerView!!) {
            text {
                title = "Name"
                required = true
                centerText = true
                tag = NAME_TAG
                value = "Some name"
            }
            dropDown<String> {
                title = "Type"
                centerText = true
                options = listOf("PC", "NPC", "Creature")
                tag = TYPE_TAG
                value = "NPC"
            }
            header {
                title = CnvUtils trans "Attributes"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            CnvUtils.AttrTags.values().forEach {
                text {
                    title = CnvUtils trans it.name
                    required = true
                    clearable = true
                    centerText = true
                    tag = ATTR_TAGS + it.ordinal
                    inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
                    value = ""
                }
            }
            header {
                title = CnvUtils trans "Stats"
                collapsible = false
                backgroundColor = Color.parseColor("#50C878")
            }
            header {
                title = CnvUtils trans "Fight stats"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            CnvUtils.FightTags.values().forEach {
                text {
                    title = CnvUtils trans it.name
                    centerText = true
                    clearable = true
                    tag = FIGHT_TAGS + it.ordinal
                    inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
                    value = ""
                }
            }
            header {
                title = CnvUtils trans "Social stats"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            CnvUtils.SocialTags.values().forEach {
                text {
                    title = CnvUtils trans it.name
                    centerText = true
                    clearable = true
                    tag = SOC_TAGS + it.ordinal
                    value = "0.0"
                }
            }
            header {
                title = CnvUtils trans "Shadow stats"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            CnvUtils.ShadowTags.values().forEach {
                text {
                    title = CnvUtils trans it.name
                    centerText = true
                    clearable = true
                    tag = SHADOW_TAGS + it.ordinal
                    value = ""
                }
            }
            header {
                title = CnvUtils trans "Extra stats"
                collapsible = true
                allCollapsed = true
                clearable = true
                tag = HEADER_TAG
            }
            textArea {
                displayTitle = false
                title = CnvUtils trans "Extra stats"
                maxLines = 10
                tag = EXTRAS_TAG
                rightToLeft = false
                value = ""
            }
            number {
                title = CnvUtils trans "Void Points"
                centerText = true
                clearable = true
                tag = VOIDPTS_TAG
                value = "0"
            }
            header {
                title = CnvUtils trans "Skills"
                collapsible = false
                backgroundColor = Color.parseColor("#50C878")
            }
            CnvUtils.skills.keys.forEach { key ->
                header {
                    title = "${key.name} skills"
                    collapsible = true
                    allCollapsed = true
                    tag = HEADER_TAG
                }
                CnvUtils.skills[key]!!.forEach { skill ->
                    number {
                        title = CnvUtils trans skill
                        centerText = true
                        clearable = true
                        tag = getSkillTag(skill)
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
                        value = "0"
                    }
                }
            }
            header {
                title = CnvUtils trans "Spells"
                collapsible = true
                allCollapsed = true
                backgroundColor = Color.parseColor("#50C878")
            }
            multiCheckBox<List<String>> {
                displayTitle = false
                tag = SPELLS_TAG
                options = spellList
                value = listOf()
            }
            header {
                title = CnvUtils trans "Other"
                collapsible = false
                backgroundColor = Color.parseColor("#50C878")
            }
            header {
                title = CnvUtils trans "Description"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            textArea {
                displayTitle = false
                title = CnvUtils trans "Description"
                maxLines = 20
                tag = DESC_TAG
                rightToLeft = false
                value = ""
            }
            header {
                title = CnvUtils trans "Equipment"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            textArea {
                displayTitle = false
                title = CnvUtils trans "Equipment"
                maxLines = 20
                tag = EQUIP_TAG
                rightToLeft = false
                value = ""
            }
            header {
                title = CnvUtils trans "Notes"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            textArea {
                displayTitle = false
                title = CnvUtils trans "Notes"
                maxLines = 20
                tag = NOTES_TAG
                rightToLeft = false
                value = ""
            }
        }
        activity?.title = "Edit character"
        formBuilder!!.attachRecyclerView(context!!, recyclerView)
        if (id != -1L) {
            DbWorker.postDbTask(context!!) { db ->
                val dchar = db.dcharDao().findById(id)
                if (dchar != null) {
                    val adapter = moshi.adapter(RpgCharacter::class.java)
                    val json = adapter.fromJson(dchar.data!!)!!
                    updateElement(NAME_TAG, dchar.name!!)
                    updateElement(TYPE_TAG, dchar.type!!)
                    updateElement(NOTES_TAG, json.notes)
                    updateElement(EQUIP_TAG, json.equipment)
                    updateElement(DESC_TAG, json.description)
                    updateElement(VOIDPTS_TAG, json.voidpts.toString())
                    updateElement(EXTRAS_TAG, json.extras)
                    json.skills.forEach {
                        updateElement(getSkillTag(it.name), it.roll)
                    }
                    updateElement(SHADOW_TAGS + CnvUtils.ShadowTags.Fear.ordinal, json.fear.toString())
                    updateElement(SHADOW_TAGS + CnvUtils.ShadowTags.Taint.ordinal, json.taint.toString())
                    updateElement(FIGHT_TAGS + CnvUtils.FightTags.DeadWounds.ordinal, json.bp.toString())
                    updateElement(FIGHT_TAGS + CnvUtils.FightTags.Damage.ordinal, json.damage.toString())
                    updateElement(FIGHT_TAGS + CnvUtils.FightTags.Attack.ordinal, json.attack.toString())
                    updateElement(
                        FIGHT_TAGS + CnvUtils.FightTags.Initiative.ordinal,
                        json.initiative.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Intelligence.ordinal,
                        json.intelligence.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Agility.ordinal,
                        json.agility.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Awareness.ordinal,
                        json.awareness.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Reflexes.ordinal,
                        json.reflexes.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Strength.ordinal,
                        json.strength.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Perception.ordinal,
                        json.perception.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Willpower.ordinal,
                        json.willpower.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Stamina.ordinal,
                        json.stamina.toString()
                    )
                    updateElement(
                        ATTR_TAGS + CnvUtils.AttrTags.Magic.ordinal,
                        json.magic.toString()
                    )
                    updateElement(SOC_TAGS + CnvUtils.SocialTags.Glory.ordinal, json.glory.toString())
                    updateElement(SOC_TAGS + CnvUtils.SocialTags.Honor.ordinal, json.honor.toString())
                    updateElement(SOC_TAGS + CnvUtils.SocialTags.Status.ordinal, json.status.toString())
                    updateElement(SPELLS_TAG, json.spells)
                }
            }
        }
    }


    fun collapseAllHeaders(value: Boolean = true) {
        formBuilder?.elements!!.forEach {
            if (it.tag == HEADER_TAG) {
                val elem = it as FormHeader
                elem.setAllCollapsed(value, formBuilder!!)
            }
        }
    }

    fun filterElements(filter: String) {
        formBuilder?.elements!!.forEach {
            if (it.title != null) {
                it.visible = filter.isEmpty() || it.title!!.toLowerCase().startsWith(filter.toLowerCase())
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.charedit_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                filterElements(filter)
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_collapse -> {
                collapseAllHeaders()
            }
            R.id.action_expand -> {
                collapseAllHeaders(false)
            }
            R.id.action_save -> {
                save()
            }
        }
        return true
    }

    private fun getSkillTag(skill: String): Int {
        if (!skillList.contains(skill)) {
            skillList.add(skill)
        }
        return SKILL_TAGS + skillList.indexOf(skill)
    }

    fun updateElement(tag: Int, value: String) {
        val element = formBuilder!!.getFormElement<BaseFormElement<String>>(tag)
        element.value = value
    }

    fun updateElement(tag: Int, value: List<String>) {
        val element = formBuilder!!.getFormElement<BaseFormElement<List<String>>>(tag)
        element.value = value
    }

    fun getElementValue(tag: Int): String {
        val element = formBuilder!!.getFormElement<BaseFormElement<String>>(tag)
        return element.value.toString()
    }

    fun getElementListValue(tag: Int): List<String> {
        val element = formBuilder!!.getFormElement<BaseFormElement<List<String>>>(tag)
        return element.value!!.toList()
    }

    fun save() {
        val adapter = moshi.adapter(RpgCharacter::class.java)
        var skills = mutableListOf<RpgSkill>()
        skillList.forEach {
            val vv = getElementValue(SKILL_TAGS + skillList.indexOf(it))
            if (vv.isNotEmpty() && vv != "0") {
                skills.add(RpgSkill(it, vv))
            }
        }
        val rpc = RpgCharacter(
            getElementValue(NAME_TAG),
            getElementValue(TYPE_TAG),
            getElementValue(FIGHT_TAGS + CnvUtils.FightTags.Initiative.ordinal),
            getElementValue(FIGHT_TAGS + CnvUtils.FightTags.Attack.ordinal),
            getElementValue(FIGHT_TAGS + CnvUtils.FightTags.Damage.ordinal),
            getElementValue(FIGHT_TAGS + CnvUtils.FightTags.DeadWounds.ordinal).toIntOrZero(),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Stamina.ordinal),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Willpower.ordinal),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Perception.ordinal),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Strength.ordinal),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Reflexes.ordinal),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Awareness.ordinal),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Agility.ordinal),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Intelligence.ordinal),
            getElementValue(ATTR_TAGS + CnvUtils.AttrTags.Magic.ordinal),
            getElementValue(SOC_TAGS + CnvUtils.SocialTags.Honor.ordinal).toFloatOrZero(),
            getElementValue(SOC_TAGS + CnvUtils.SocialTags.Glory.ordinal).toFloatOrZero(),
            getElementValue(SOC_TAGS + CnvUtils.SocialTags.Status.ordinal).toFloatOrZero(),
            getElementValue(SHADOW_TAGS + CnvUtils.ShadowTags.Fear.ordinal).toIntOrZero(),
            getElementValue(SHADOW_TAGS + CnvUtils.ShadowTags.Taint.ordinal).toFloatOrZero(),
            skills,
            getElementValue(EXTRAS_TAG),
            getElementValue(EQUIP_TAG),
            getElementValue(DESC_TAG),
            getElementValue(NOTES_TAG),
            getElementValue(VOIDPTS_TAG).toIntOrZero(),
            getElementListValue(SPELLS_TAG)
        )
        DbWorker.postDbTask(context!!) { db ->
            if (id != -1L) {
                val dc = DChar(id, rpc.name, rpc.type, adapter.toJson(rpc))
                db.dcharDao().save(dc)
            } else {
                val dc = DChar(null, rpc.name, rpc.type, adapter.toJson(rpc))
                db.dcharDao().insertAll(dc)
            }

            activity?.runOnUiThread {
                MiscUtils.hideKeyboard(context!!, recyclerView!!)
                router?.charlist()!!.launch()
            }
        }
    }


}