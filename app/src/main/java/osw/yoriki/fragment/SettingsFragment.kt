package osw.yoriki.fragment

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import osw.yoriki.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
    }

}