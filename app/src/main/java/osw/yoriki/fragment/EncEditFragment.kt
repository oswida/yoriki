package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.squareup.moshi.Moshi
import com.thejuki.kformmaster.helper.*
import com.thejuki.kformmaster.model.BaseFormElement
import com.thejuki.kformmaster.model.FormLabelElement
import com.thejuki.kformmaster.model.FormMultiLineEditTextElement
import com.thejuki.kformmaster.model.FormSingleLineEditTextElement
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.*
import osw.yoriki.util.DialogUtils

@RouterPath(name = "encedit")
class EncEditFragment : Fragment() {

    private val DESC_TAG = 200
    private val NAME_TAG = 300
    private val PTS_TAG = 400

    @Argument
    var id: Long = -1L

    private var recyclerView: RecyclerView? = null
    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private var formBuilder: FormBuildHelper? = null
    val moshi = Moshi.Builder().build()
    private val participants = mutableListOf<Long>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.enc_edit, container, false)
        recyclerView = result.findViewById(R.id.formview)
        initForm()
        return result
    }

    fun initForm() {
        formBuilder = form(context!!, recyclerView!!) {
            text {
                title = "Name"
                required = true
                tag = NAME_TAG
                value = "Some encounter name"
            }
            header {
                title = "Description"
                value = ""
            }
            textArea {
                displayTitle = false
                maxLines = 10
                tag = DESC_TAG
                rightToLeft = false
                value = ""
            }
            header {
                title = "Participants"
            }
            label {
                title = ""
                tag = PTS_TAG
            }
            button {
                value = "Add participant"
                valueObservers.add { _, _ ->
                    DbWorker.postDbTask(context!!) { db ->
                        val chars = db.dcharDao().getAll()
                        if (chars.isNotEmpty()) {
                            DialogUtils.select(context!!, "Select character",
                                chars.map { it.name!! }) { pos, name ->
                                participants.add(chars[pos].id!!)
                                updateParticipantsLabel()
                            }
                        }
                    }
                }
            }
            button {
                value = "Remove participant"
                valueObservers.add { _, _ ->
                    DbWorker.postDbTask(context!!) { db ->
                        val chars = participants.map {
                            db.dcharDao().findById(it)
                        }
                        if (chars.isNotEmpty()) {
                            DialogUtils.select(context!!, "Select character",
                                chars.map { it!!.name!! }) { pos, name ->
                                participants.remove(chars[pos]!!.id!!)
                                updateParticipantsLabel()
                            }
                        }
                    }
                }
            }
        }
        activity?.title = "Edit encounter"
        formBuilder!!.attachRecyclerView(context!!, recyclerView)
        if (id != -1L) {
            DbWorker.postDbTask(context!!) { db ->
                val enc = db.encounterDao().findById(id)
                val adapter = moshi.adapter(RpgEncounter::class.java)
                val rpc = adapter.fromJson(enc!!.data)
                updateElement(NAME_TAG, enc!!.title!!)
                updateElement(DESC_TAG, rpc!!.description)
                participants.addAll(rpc.chars.map { it.id })
                updateParticipantsLabel()
            }
        }
    }

    fun updateElement(tag: Int, value: String) {
        val element = formBuilder!!.getFormElement<BaseFormElement<String>>(tag)
        element.value = value
    }

    fun updateParticipantsLabel() {
        val elem = formBuilder!!.getFormElement<FormLabelElement>(PTS_TAG)
        DbWorker.postDbTask(context!!) { db ->
            val text = participants.map {
                val dc = db.dcharDao().findById(it)
                if (dc != null) {
                    dc.name!!
                } else {
                    ""
                }
            }.joinToString(", ")
            activity?.runOnUiThread {
                elem.title = text
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.encedit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                save()
            }
        }
        return true
    }

    fun save() {
        val adapter = moshi.adapter(RpgEncounter::class.java)
        val dcadapter = moshi.adapter(RpgCharacter::class.java)
        val chars = mutableListOf<RpgParticipant>()
        DbWorker.postDbTask(context!!) { db ->
            participants.forEach {
                val dc = db.dcharDao().findById(it)
                if (dc != null) {
                    val data = dcadapter.fromJson(dc.data)
                    val rpc = RpgParticipant(
                        it, 0, 0, 0, 0, data!!.bp, "", listOf(), data
                    )
                    chars.add(rpc)
                }
            }
            val description = formBuilder!!.getFormElement<FormMultiLineEditTextElement>(DESC_TAG).value
            val name = formBuilder!!.getFormElement<FormSingleLineEditTextElement>(NAME_TAG).value
            val tosave = RpgEncounter(0, 0, 0, 0, description!!, chars)
            if (id != -1L) {
                val item = db.encounterDao().findById(id)
                item!!.title = name
                item.data = adapter.toJson(tosave)
                db.encounterDao().save(item)
                activity?.runOnUiThread {
                    router?.enclist()!!.launch()
                }
            } else {
                val item = Encounter(null, name, adapter.toJson(tosave))
                db.encounterDao().insertAll(item)
                activity?.runOnUiThread {
                    router?.enclist()!!.launch()
                }
            }
        }

    }


}