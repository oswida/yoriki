package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.RouterPath
import com.google.android.material.floatingactionbutton.FloatingActionButton
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.DbWorker
import osw.yoriki.model.EncounterItem
import osw.yoriki.model.EncounterModel
import osw.yoriki.util.DialogUtils

@RouterPath(name = "enclist")
class EnclistFragment : Fragment(), FlexibleAdapter.OnItemClickListener, FlexibleAdapter.OnItemSwipeListener {


    private var recyclerView: RecyclerView? = null
    private lateinit var model: EncounterModel
    private lateinit var fragmentAdapter: FlexibleAdapter<EncounterItem>
    private val router: MoriRouter get() = (activity as MainActivity).router
    private var filter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.enclist_fragment, container, false)
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(EncounterModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = true
        fragmentAdapter.addListener(this)
        model.itemList.observe(this, Observer<List<EncounterItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        val fab = result.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            router?.encedit(-1L).launch()
        }
        refresh()
        return result
    }

    private fun refresh() {
        activity?.runOnUiThread {
            model.refresh(filter)
            activity?.title = "Encounters"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.enclist_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                refresh()
                return true
            }
        })
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)
        router.encview(item!!.item.id!!).launch()
        return true
    }

    override fun onActionStateChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
    }

    override fun onItemSwipe(position: Int, direction: Int) {
        val item = fragmentAdapter.getItem(position)
        when (direction) {
            8 -> {
                DialogUtils.confirm(
                    context!!,
                    "Delete encounter",
                    "Do you want to delete encounter ${item!!.item.title}?"
                ) {
                    DbWorker.postDbTask(context!!) { db ->
                        db.encounterDao().delete(item.item)
                        model.refresh(filter)
                    }
                }
                model.refresh(filter)
            }
            4 -> {
                router.encedit(item!!.item.id!!).launch()
            }
        }
    }

}