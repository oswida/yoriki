package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.squareup.moshi.Moshi
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.DbWorker
import osw.yoriki.db.RpgEncounter
import osw.yoriki.db.RpgParticipant
import osw.yoriki.db.RpgSpell
import osw.yoriki.model.EncViewItem
import osw.yoriki.model.EncViewModel
import osw.yoriki.util.DialogUtils
import osw.yoriki.util.FileUtils
import osw.yoriki.util.MiscUtils
import osw.yoriki.util.TextUtils

@RouterPath(name = "encview")
class EncViewFragment : Fragment(), FlexibleAdapter.OnItemClickListener, FlexibleAdapter.OnItemSwipeListener {

    @Argument
    var id = -1L

    private var recyclerView: RecyclerView? = null
    private lateinit var model: EncViewModel
    private lateinit var fragmentAdapter: FlexibleAdapter<EncViewItem>
    private val router: MoriRouter get() = (activity as MainActivity).router
    private var encounter: RpgEncounter? = null
    private val moshi = Moshi.Builder().build()
    private val adapter = moshi.adapter(RpgEncounter::class.java)

    var activeParticipant: RpgParticipant? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.encview_fragment, container, false)
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(EncViewModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = true
        fragmentAdapter.addListener(this)
        model.itemList.observe(this, Observer<List<EncViewItem>> {
            fragmentAdapter.updateDataSet(it)
        })
//        val fab = result.findViewById<FloatingActionButton>(R.id.fab)
//        fab.setOnClickListener {
//            router?.encedit(-1L).launch()
//        }
        refresh()
        return result
    }

    private fun refresh() {
        DbWorker.postDbTask(context!!) { db ->
            val enc = db.encounterDao().findById(id)
            if (enc != null) {
                encounter = adapter.fromJson(enc.data)
                activity?.runOnUiThread {
                    activity?.title = enc.title
                    model.refresh(encounter!!.chars, this)
                }
            }
        }
    }

    private fun saveEncounter() {
        DbWorker.postDbTask(context!!) { db ->
            val enc = db.encounterDao().findById(id)
            if (enc != null) {
                enc.data = adapter.toJson(encounter)
                db.encounterDao().save(enc)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.encview_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_spells -> {
                val list = context!!.assets.list("spells")
                    .sorted()
                    .map {
                        TextUtils.removeJsonExts(it)
                    }
                DialogUtils.selectWithSearch(context!!, "Spells", list) { pos, name ->
                    val text = FileUtils.textFromAsset(context!!.assets, "spells/${name}.json")
                    val adp = moshi.adapter(RpgSpell::class.java)
                    DialogUtils.mdinfo(context!!, name, adp.fromJson(text)!!.toMarkdown())
                }
            }
            R.id.action_notes -> {
                DbWorker.postDbTask(context!!) { db ->
                    val list = db.noteDao().getAll().map { it.title!! }
                    DialogUtils.selectWithSearch(context!!, "Notes", list) { pos, name ->
                        val note = db.noteDao().findByTitle(name)
                        activity?.runOnUiThread {
                            DialogUtils.mdinfo(context!!, name, note!!.content!!)
                        }
                    }
                }
            }
        }
        return true
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)
        DialogUtils.select(
            context!!, "", listOf(
                "Initiative", "Status", "Actions", "Change name"
            )
        ) { pos, name ->
            when (pos) {
                0 -> {
                    DialogUtils.selectInt(context!!, "Initiative", 0, 100, 0) {
                        item!!.item.initiative = it
                        model.refresh(encounter!!.chars, this)
                        saveEncounter()
                    }
                }
                1 -> {
                    DialogUtils.inputString(context!!, "Status", item!!.item.state) {
                        item.item.state = it
                        model.refresh(encounter!!.chars, this)
                        saveEncounter()
                    }
                }
                2 -> {
                    val selected = mutableListOf<Int>()
                    item!!.item.actions.forEach {
                        val idx = MiscUtils.charActions.indexOf(it)
                        if (idx >= 0) {
                            selected.add(idx)
                        }
                    }
                    DialogUtils.selectMultiple(
                        context!!,
                        "Actions",
                        MiscUtils.charActions,
                        selected.toIntArray()
                    ) {
                        item.item.actions = it
                        model.refresh(encounter!!.chars, this)
                        saveEncounter()
                    }
                }
                3 -> {
                    DialogUtils.inputString(context!!, "Change name", item!!.item.stats.name) {
                        item.item.stats.name = it
                        model.refresh(encounter!!.chars, this)
                        saveEncounter()
                    }
                }
            }
        }
        return true
    }

    override fun onActionStateChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
    }

    override fun onItemSwipe(position: Int, direction: Int) {
        val item = fragmentAdapter.getItem(position)
        when (direction) {
            8 -> {
                DialogUtils.mdinfo(context!!, "Info", item!!.item.stats.toSinglePageMarkdown(true))
                model.refresh(encounter!!.chars, this)
            }
            4 -> {
                activeParticipant = item!!.item
                model.refresh(encounter!!.chars, this)
            }
        }
    }

    fun setBodyPoints(item: RpgParticipant) {
        DialogUtils.selectInt(
            context!!, "Body Points",
            0, item.stats.bp, item.bp
        ) {
            item.bp = it
            model.refresh(encounter!!.chars, this)
            saveEncounter()
        }
    }

    fun setActionModifier(item: RpgParticipant) {
        DialogUtils.inputInt(context!!, "Action modifier", item.actMod) {
            item.actMod = it
            model.refresh(encounter!!.chars, this)
            saveEncounter()
        }
    }

    fun setAttModifier(item: RpgParticipant) {
        DialogUtils.inputInt(context!!, "Attack modifier", item.attMod) {
            item.attMod = it
            model.refresh(encounter!!.chars, this)
            saveEncounter()
        }
    }

    fun setDefModifier(item: RpgParticipant) {
        DialogUtils.inputInt(context!!, "Defense modifier", item.defMod) {
            item.defMod = it
            model.refresh(encounter!!.chars, this)
            saveEncounter()
        }
    }

    fun setActive(item: RpgParticipant) {
        activeParticipant = item
        model.refresh(encounter!!.chars, this)
    }

}