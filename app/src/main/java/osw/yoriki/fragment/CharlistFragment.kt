package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.RouterPath
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.squareup.moshi.Moshi
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.DChar
import osw.yoriki.db.DbWorker
import osw.yoriki.db.RpgCharacter
import osw.yoriki.model.DCharItem
import osw.yoriki.model.DCharModel
import osw.yoriki.util.DialogUtils

@RouterPath(name = "charlist")
class CharlistFragment : Fragment(),
    FlexibleAdapter.OnItemClickListener,
    FlexibleAdapter.OnItemSwipeListener,
    FlexibleAdapter.OnItemLongClickListener {


    private var recyclerView: RecyclerView? = null
    private lateinit var model: DCharModel
    private lateinit var fragmentAdapter: FlexibleAdapter<DCharItem>
    private val router: MoriRouter get() = (activity as MainActivity).router
    private var filter = ""
    val moshi = Moshi.Builder().build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.charlist_fragment, container, false)
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(DCharModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = true
        fragmentAdapter.addListener(this)
        model.itemList.observe(this, Observer<List<DCharItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        val fab = result.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            router.charedit(-1L).launch()
        }
        refresh()
        return result
    }

    private fun refresh() {
        activity?.runOnUiThread {
            model.refresh(filter, this)
            activity?.title = "Characters"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.charlist_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                refresh()
                return true
            }
        })
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)
        router.charview(item!!.item.id!!).launch()
//        val adapter = moshi.adapter(RpgCharacter::class.java)
//        router.markdown(
//            item!!.item.name!!,
//            adapter.fromJson(item.item.data)!!.toSinglePageMarkdown(true)
//        ).launch()
        return true
    }

    override fun onActionStateChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
    }

    override fun onItemSwipe(position: Int, direction: Int) {
        val item = fragmentAdapter.getItem(position)
        when (direction) {
            8 -> {
                DialogUtils.confirm(
                    context!!,
                    "Delete character",
                    "Do you want to delete character ${item!!.item.name}?"
                ) {
                    DbWorker.postDbTask(context!!) { db ->
                        db.dcharDao().delete(item.item)
                        model.refresh(filter, this)
                    }
                }
                model.refresh(filter, this)
            }
            4 -> {
                router.charedit(item!!.item.id!!).launch()
            }
            else -> model.refresh(filter, this)
        }
    }

    override fun onItemLongClick(position: Int) {
        val item = fragmentAdapter.getItem(position)
        DialogUtils.select(context!!, item!!.item.name!!, listOf("Edit notes")) { pos, name ->
            val adapter = moshi.adapter(RpgCharacter::class.java)
            val rpc = adapter.fromJson(item.item.data)
            when(pos) {
                0 -> {
                    router.markdownEdit(rpc!!.name, rpc.notes, MdEditType.CharNotes, item.item.id!!).launch()
                }
            }
        }
    }

    fun showNotes(item: DChar) {
        val adapter = moshi.adapter(RpgCharacter::class.java)
        val rpc = adapter.fromJson(item.data)
        DialogUtils.mdinfo(context!!, item.name!!, rpc!!.notes)
    }
}