package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import br.tiagohm.markdownview.MarkdownView
import br.tiagohm.markdownview.css.styles.Github
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.options.MutableDataSet
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.theme.GithubDark
import osw.yoriki.theme.GithubLight
import osw.yoriki.util.DialogUtils
import osw.yoriki.util.PrefUtils

@RouterPath(name = "markdown")
class MarkdownFragment : Fragment() {

    @Argument
    var dataTitle: String? = null

    @Argument
    var data: String? = null

    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private lateinit var contentView: MarkdownView
    private var filter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.md_fragment, container, false)
        contentView = result.findViewById(R.id.contentView)
        contentView.setOnLongClickListener {
            val bar = (activity as MainActivity).supportActionBar!!
            if (bar.isShowing) {
                bar.hide()
            } else {
                bar.show()
            }
            true
        }
        refresh()
        return result
    }


    override fun onDestroy() {
        val bar = (activity as MainActivity).supportActionBar!!
        bar.show()
        super.onDestroy()
    }

    private fun refresh() {
        var theme: Github = GithubLight(context!!)
        if (PrefUtils.isNightmode(context!!)) {
            theme = GithubDark(context!!)
        }
//        theme.addRule(
//            "body",
//            "font-size: " +
//                    SysUtils.dipToPix(context!!, PrefHelper.getWebViewFont(context!!).toFloat()) + "px"
//        )
        contentView.addStyleSheet(theme)
        contentView.loadMarkdown(data)
        activity?.title = dataTitle
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.md_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                if (filter.isNotBlank()) {
                    contentView.findAllAsync(filter)
                } else {
                    contentView.clearMatches()
                }
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_toc -> {
                val options = MutableDataSet()
                val parser = Parser.builder(options).build()
                val doc = parser.parse(data)
                val list = mutableListOf<String>()
                doc.childIterator.forEach {
                    if (it.nodeName == "Heading") {
                        list.add(it.chars.toString().replace("#", "."))
                    }
                }
                DialogUtils.select(context!!, "Table of Contents", list) { pos, name ->
                    contentView.findAllAsync(name.replace(".", "").trim())
                    contentView.clearMatches()
                }
            }
        }
       return true
    }

}