package osw.yoriki.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.RouterPath
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.model.MapItem
import osw.yoriki.model.MapModel

@RouterPath(name = "maplist")
class MaplistFragment : Fragment(), FlexibleAdapter.OnItemClickListener {

    private var recyclerView: RecyclerView? = null
    private lateinit var model: MapModel
    private lateinit var fragmentAdapter: FlexibleAdapter<MapItem>
    private val router: MoriRouter get() = (activity as MainActivity).router
    private var filter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.maplist_fragment, container, false)
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(MapModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = false
        fragmentAdapter.addListener(this)
        model.itemList.observe(this, Observer<List<MapItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        refresh()
        return result
    }

    private fun refresh() {
        activity?.runOnUiThread {
            model.refresh(filter)
            activity?.title = "Maps"
        }
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)
        router.assetImage(item!!.path).launch()
        return true
    }


//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        inflater.inflate(R.menu.todo_menu, menu)
//        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
//        if (!filter.isEmpty()) {
//            searchView.setQuery(filter, false)
//        }
//        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(query: String?): Boolean {
//                return true
//            }
//
//            override fun onQueryTextChange(newText: String?): Boolean {
//                var tt = newText
//                if (tt === null) {
//                    tt = ""
//                }
//                filter = tt
//                refresh()
//                return true
//            }
//        })
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item?.itemId) {
//            R.id.action_archive_view -> {
//                val fn = filename!!.replace(".${Consts.TODO_EXT}", ".${Consts.TODO_ARCHIVE_EXT}")
//                router?.doneView(fn)!!.launch()
//            }
//            R.id.action_archive_all -> {
//                DialogHelper.confirm(
//                    context,
//                    "Archive all completed tasks",
//                    "Please confirm archiving of all completed tasks", {
//                        TodoHelper.archiveAll(context!!, filename!!)
//                        refresh()
//                    },
//                    {})
//            }
//            R.id.action_sort -> {
//                DialogHelper.select(context!!, "", listOf("Creation date", "Priority", "Text")) { pos, _ ->
//                    when (pos) {
//                        0 -> TodoHelper.sort(context!!, filename!!, TodoHelper.SORT_CREATION_DATE, true)
//                        1 -> TodoHelper.sort(context!!, filename!!, TodoHelper.SORT_PRIO)
//                        else -> TodoHelper.sort(context!!, filename!!, TodoHelper.SORT_TEXT)
//                    }
//                    refresh()
//                }
//            }
//        }
//        return true
//    }

}