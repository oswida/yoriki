package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.RouterPath
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.model.SpellItem
import osw.yoriki.model.SpellModel

@RouterPath(name = "spelllist")
class SpelllistFragment : Fragment(), FlexibleAdapter.OnItemClickListener {

    private var recyclerView: RecyclerView? = null
    private lateinit var model: SpellModel
    private lateinit var fragmentAdapter: FlexibleAdapter<SpellItem>
    private val router: MoriRouter get() = (activity as MainActivity).router
    private var filter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.maplist_fragment, container, false)
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(SpellModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = false
        fragmentAdapter.addListener(this)
        model.itemList.observe(this, Observer<List<SpellItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        refresh()
        return result
    }

    private fun refresh() {
        activity?.runOnUiThread {
            model.refresh(filter)
            activity?.title = "Spells"
        }
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)
        router.assetJson(item!!.path, JsonAssetType.SPELL).launch()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.spell_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                refresh()
                return true
            }
        })
    }

}