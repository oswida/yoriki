package osw.yoriki.fragment

import android.os.Bundle
import android.text.InputType
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.annotation.RouterPath
import com.squareup.moshi.Moshi
import com.thejuki.kformmaster.helper.*
import com.thejuki.kformmaster.model.BaseFormElement
import com.thejuki.kformmaster.model.FormHeader
import osw.yoriki.R
import osw.yoriki.db.DChar
import osw.yoriki.db.DbWorker
import osw.yoriki.db.RpgCharacter
import osw.yoriki.db.RpgSkill
import osw.yoriki.util.CnvUtils
import osw.yoriki.util.CnvUtils.diceFromString
import osw.yoriki.util.CnvUtils.skills
import osw.yoriki.util.DialogUtils
import osw.yoriki.util.toIntOrZero


@RouterPath(name = "charConvert")
class CharConvertFragment : Fragment() {

    private var recyclerView: RecyclerView? = null
    private var filter = ""
    private var formBuilder: FormBuildHelper? = null
    private var data = mutableMapOf<String, String>()
    private val moshi = Moshi.Builder().build()
    private val skillList = mutableListOf<String>()
    private val ringMap = mapOf(
        CnvUtils.AttrTags.Strength to CnvUtils.RingTags.WaterRing,
        CnvUtils.AttrTags.Perception to CnvUtils.RingTags.WaterRing,
        CnvUtils.AttrTags.Reflexes to CnvUtils.RingTags.AirRing,
        CnvUtils.AttrTags.Awareness to CnvUtils.RingTags.AirRing,
        CnvUtils.AttrTags.Stamina to CnvUtils.RingTags.EarthRing,
        CnvUtils.AttrTags.Willpower to CnvUtils.RingTags.EarthRing,
        CnvUtils.AttrTags.Agility to CnvUtils.RingTags.FireRing,
        CnvUtils.AttrTags.Intelligence to CnvUtils.RingTags.FireRing
    )

    private val HEADER_TAG = 200000
    private val NAME_TAG = 200005
    private val TYPE_TAG = 200006
    private val ATTR_TAGS = 100
    private val FIGHT_TAGS = 200
    private val SKILL_TAGS = 500
    private val RING_TAGS = 600

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.dlg_char_convert, container, false)
        recyclerView = result.findViewById(R.id.formview)
        initForm()
        return result
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.charedit_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                filterElements(filter)
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_collapse -> {
                collapseAllHeaders()
            }
            R.id.action_expand -> {
                collapseAllHeaders(false)
            }
            R.id.action_save -> {
                convert()
            }
        }
        return true
    }

    fun initForm() {
        skillList.clear()
        formBuilder = form(context!!, recyclerView!!) {
            text {
                title = "Name"
                required = true
                centerText = true
                tag = NAME_TAG
            }
            dropDown<String> {
                title = "Type"
                centerText = true
                options = listOf("PC", "NPC", "Creature")
                tag = TYPE_TAG
            }
            header {
                title = "Rings"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            CnvUtils.RingTags.values().forEach {
                number {
                    title = it.name
                    required = true
                    centerText = true
                    tag = RING_TAGS + it.ordinal
                    value = "2"
                }
            }
            header {
                title = "Attributes"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            CnvUtils.AttrTags.values().forEach {
                number {
                    title = it.name
                    required = true
                    centerText = true
                    tag = ATTR_TAGS + it.ordinal
                    value = "0"
                }
            }
            header {
                title = "Fight Stats"
                collapsible = true
                allCollapsed = true
                tag = HEADER_TAG
            }
            CnvUtils.FightTags.values().forEach {
                text {
                    title = it.name
                    centerText = true
                    tag = FIGHT_TAGS + it.ordinal
                    inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
                    value = if (it.ordinal == CnvUtils.FightTags.DeadWounds.ordinal) {
                        "0"
                    } else {
                        ""
                    }
                }
            }
            skills.keys.forEach { key ->
                header {
                    title = "${key.name} skills"
                    collapsible = true
                    allCollapsed = true
                    tag = HEADER_TAG
                }
                skills[key]!!.forEach { skill ->
                    number {
                        title = skill
                        centerText = true
                        tag = getSkillTag(skill)
                        value = "0"
                    }
                }
            }
        }
        activity?.title = "L5K Convert"
        formBuilder!!.attachRecyclerView(context!!, recyclerView)
    }

    private fun getSkillTag(skill: String): Int {
        if (!skillList.contains(skill)) {
            skillList.add(skill)
        }
        return SKILL_TAGS + skillList.indexOf(skill)
    }

    fun getElementValue(tag: Int): String {
        val element = formBuilder!!.getFormElement<BaseFormElement<String>>(tag)
        return element.value.toString()
    }

    fun computeAttr(attr: CnvUtils.AttrTags): String {
        val elem = getElementValue(ATTR_TAGS + attr.ordinal)
        val ring = getElementValue(RING_TAGS + ringMap[attr]!!.ordinal)
        return CnvUtils.attrString(elem.toIntOrZero(), diceFromString(ring))
    }

    fun computeSkill(name: String, value: String): String {
        if (value.isNotEmpty() && value != "0") {
            val attr = CnvUtils.findAttrForSkill(name)
            val attrvalue = computeAttr(attr!!)
            if (CnvUtils.valueIsRollKeep(value)) {
                return CnvUtils.cnvRollString(value)
            } else {
                return CnvUtils.skillString(
                    value.toIntOrZero(),
                    CnvUtils.diceFromString(attrvalue)
                )
            }
        }
        return ""
    }

    fun computeFightStat(stat: CnvUtils.FightTags): String {
        val value = getElementValue(FIGHT_TAGS + stat.ordinal)
        when (stat) {
            CnvUtils.FightTags.Damage, CnvUtils.FightTags.Attack, CnvUtils.FightTags.Initiative -> {
                return CnvUtils.cnvRollString(value)
            }
            CnvUtils.FightTags.DeadWounds -> {
                return (value.toIntOrZero() / 2).toString()
            }
        }
    }

    fun convert() {
        DialogUtils.confirm(context!!, "Save", "Save character to database?") {
            val rpc = RpgCharacter(
                getElementValue(NAME_TAG),
                getElementValue(TYPE_TAG),
                computeFightStat(CnvUtils.FightTags.Initiative),
                computeFightStat(CnvUtils.FightTags.Attack),
                computeFightStat(CnvUtils.FightTags.Damage),
                computeFightStat(CnvUtils.FightTags.DeadWounds).toIntOrZero(),
                computeAttr(CnvUtils.AttrTags.Stamina),
                computeAttr(CnvUtils.AttrTags.Willpower),
                computeAttr(CnvUtils.AttrTags.Perception),
                computeAttr(CnvUtils.AttrTags.Strength),
                computeAttr(CnvUtils.AttrTags.Reflexes),
                computeAttr(CnvUtils.AttrTags.Awareness),
                computeAttr(CnvUtils.AttrTags.Agility),
                computeAttr(CnvUtils.AttrTags.Intelligence),
                "-"
            )
            CnvUtils.skills.keys.forEach { key ->
                skills[key]!!.forEach { skill ->
                    val elem = getElementValue(SKILL_TAGS + skillList.indexOf(skill))
                    if (elem.isNotBlank()) {
                        val skillval = computeSkill(skill, elem)
                        if (skillval.isNotBlank()) {
                            rpc.skills.add(RpgSkill(skill,skillval))
                        }
                    }
                }
            }
            val adapter = moshi.adapter(RpgCharacter::class.java)
            DbWorker.postDbTask(context!!) { db ->
                val dc = DChar(null, rpc.name, rpc.type, adapter.toJson(rpc))
                db.dcharDao().insertAll(dc)
            }
            DialogUtils.info(context!!, rpc.name, adapter.indent(" ").toJson(rpc))
        }
    }

    fun filterElements(filter: String) {
        formBuilder?.elements!!.forEach {
            if (it.title != null) {
                it.visible = filter.isEmpty() || it.title!!.toLowerCase().startsWith(filter.toLowerCase())
            }
        }
    }

    fun collapseAllHeaders(value: Boolean = true) {
        formBuilder?.elements!!.forEach {
            if (it.tag == HEADER_TAG) {
                val elem = it as FormHeader
                elem.setAllCollapsed(value, formBuilder!!)
            }
        }
    }


}