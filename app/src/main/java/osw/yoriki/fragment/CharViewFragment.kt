package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import br.tiagohm.markdownview.MarkdownView
import br.tiagohm.markdownview.css.styles.Github
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.squareup.moshi.Moshi
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.options.MutableDataSet
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.DbWorker
import osw.yoriki.db.RpgCharacter
import osw.yoriki.db.RpgSpell
import osw.yoriki.theme.GithubDark
import osw.yoriki.theme.GithubLight
import osw.yoriki.util.DialogUtils
import osw.yoriki.util.FileUtils
import osw.yoriki.util.PrefUtils

@RouterPath(name = "charview")
class CharViewFragment : Fragment() {

    @Argument
    var id = -1L

    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private lateinit var contentView: MarkdownView
    private val moshi = Moshi.Builder().build()
    private var data = ""
    private var rpc: RpgCharacter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.md_fragment, container, false)
        contentView = result.findViewById(R.id.contentView)
        contentView.setOnLongClickListener {
            val bar = (activity as MainActivity).supportActionBar!!
            if (bar.isShowing) {
                bar.hide()
            } else {
                bar.show()
            }
            true
        }
        refresh()
        return result
    }

    override fun onDestroy() {
        val bar = (activity as MainActivity).supportActionBar!!
        bar.show()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.charview_menu, menu)
    }

    private fun refresh() {
        DbWorker.postDbTask(context!!) { db ->
            val dchar = db.dcharDao().findById(id)
            if (dchar != null) {
                var theme: Github = GithubLight(context!!)
                if (PrefUtils.isNightmode(context!!)) {
                    theme = GithubDark(context!!)
                }
//        theme.addRule(
//            "body",
//            "font-size: " +
//                    SysUtils.dipToPix(context!!, PrefHelper.getWebViewFont(context!!).toFloat()) + "px"
//        )
                contentView.addStyleSheet(theme)
                val adapter = moshi.adapter(RpgCharacter::class.java)
                rpc = adapter.fromJson(dchar.data)!!
                data = rpc!!.toSinglePageMarkdown(true)
                activity?.runOnUiThread {
                    contentView.loadMarkdown(data)
                    activity?.title = dchar.name
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_toc -> {
                val options = MutableDataSet()
                val parser = Parser.builder(options).build()
                val doc = parser.parse(data)
                val list = mutableListOf<String>()
                doc.childIterator.forEach {
                    if (it.nodeName == "Heading") {
                        list.add(it.chars.toString().replace("#", "."))
                    }
                }
                DialogUtils.select(context!!, "Table of Contents", list) { pos, name ->
                    contentView.findAllAsync(name.replace(".", "").trim())
                    contentView.clearMatches()
                }
            }
            R.id.action_charops -> {
                DialogUtils.select(context!!, "", listOf("Edit note", "Show spell")) { pos, name ->
                    when (pos) {
                        0 -> {
                            router?.markdownEdit(
                                rpc!!.name, rpc!!.notes,
                                MdEditType.CharNotes, id
                            )!!.launch()
                        }
                        1 -> {
                            if (rpc!!.spells.isNotEmpty()) {
                                DialogUtils.select(context!!, "", rpc!!.spells) { selpos, selname ->
                                    val text = FileUtils.textFromAsset(context!!.assets,
                                        "spells/${selname}.json")
                                    val ad = moshi.adapter(RpgSpell::class.java)
                                    DialogUtils.mdinfo(context!!, selname, ad.fromJson(text)!!.toMarkdown())
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }
}