package osw.yoriki.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.tiagohm.markdownview.MarkdownView
import br.tiagohm.markdownview.css.styles.Github
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.squareup.moshi.Moshi
import osw.yoriki.MainActivity
import osw.yoriki.db.RpgSpell
import osw.yoriki.theme.GithubDark
import osw.yoriki.theme.GithubLight
import osw.yoriki.util.FileUtils
import osw.yoriki.util.PrefUtils


enum class JsonAssetType {
    NONE,
    SPELL
}

@RouterPath(name = "assetJson")
class AssetJsonFragment : Fragment() {

    @Argument
    var path = ""

    @Argument
    var type = JsonAssetType.NONE

    private lateinit var contentView: MarkdownView
    private val moshi = Moshi.Builder().build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val result = inflater.inflate(osw.yoriki.R.layout.yaml_fragment, container, false)
        contentView = result.findViewById(osw.yoriki.R.id.contentView)
        contentView.setOnLongClickListener {
            val bar = (activity as MainActivity).supportActionBar!!
            if (bar.isShowing) {
                bar.hide()
            } else {
                bar.show()
            }
            true
        }
        refresh()
        return result
    }

    fun refresh() {
        var theme: Github = GithubLight(context!!)
        if (PrefUtils.isNightmode(context!!)) {
            theme = GithubDark(context!!)
        }
//        theme.addRule(
//            "body",
//            "font-size: " +
//                    SysUtils.dipToPix(context!!, PrefHelper.getWebViewFont(context!!).toFloat()) + "px"
//        )
        contentView.addStyleSheet(theme)
        when (type) {
            JsonAssetType.SPELL -> {
                val jsonAdapter = moshi.adapter<RpgSpell>(RpgSpell::class.java)
                val spell = jsonAdapter.fromJson(FileUtils.textFromAsset(context!!.assets, path))!!
                contentView.loadMarkdown(spell.toMarkdown())
            }
        }

    }
}