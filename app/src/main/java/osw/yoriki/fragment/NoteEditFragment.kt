package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.squareup.moshi.Moshi
import com.thejuki.kformmaster.helper.*
import com.thejuki.kformmaster.model.FormMultiLineEditTextElement
import com.thejuki.kformmaster.model.FormSingleLineEditTextElement
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.DbWorker
import osw.yoriki.db.Note

@RouterPath(name = "noteedit")
class NoteEditFragment : Fragment() {

    val TAG_TITLE = 100
    val TAG_TAGS = 101
    val TAG_CONTENT = 102

    @Argument
    var id = -1L

    private var recyclerView: RecyclerView? = null
    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private var formBuilder: FormBuildHelper? = null
    val moshi = Moshi.Builder().build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.note_edit, container, false)
        recyclerView = result.findViewById(R.id.formview)
        initForm()
        return result
    }

    fun initForm() {
        formBuilder = form(context!!, recyclerView!!) {
            text {
                title = "Title"
                required = true
                centerText = true
                tag = TAG_TITLE
            }
            text {
                title = "Tags"
                centerText = true
                tag = TAG_TAGS
            }
            header {
                title = "Content"
            }
            textArea {
                maxLines = 25
                displayTitle = false
                rightToLeft = false
                title = null
                tag = TAG_CONTENT
            }
        }
        activity?.title = "Edit note"
        formBuilder!!.attachRecyclerView(context!!, recyclerView)
        if (id != -1L) {
            DbWorker.postDbTask(context!!) { db ->
                val note = db.noteDao().findById(id)
                if (note != null) {
                    val ttl = formBuilder!!.getFormElement<FormSingleLineEditTextElement>(TAG_TITLE)
                    ttl.value = note.title
                    val tags = formBuilder!!.getFormElement<FormSingleLineEditTextElement>(TAG_TAGS)
                    tags.value = note.tags
                    val cnt = formBuilder!!.getFormElement<FormMultiLineEditTextElement>(TAG_CONTENT)
                    cnt.value = note.content
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.noteedit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                DbWorker.postDbTask(context!!) { db ->
                    val ttl =
                        formBuilder!!.getFormElement<FormSingleLineEditTextElement>(TAG_TITLE)
                    val tags =
                        formBuilder!!.getFormElement<FormSingleLineEditTextElement>(TAG_TAGS)
                    val cnt =
                        formBuilder!!.getFormElement<FormMultiLineEditTextElement>(TAG_CONTENT)
                    var note = db.noteDao().findById(id)
                    if (note != null) {
                        note.title = ttl.value
                        note.tags = tags.value
                        note.content = cnt.value
                        db.noteDao().save(note)
                    } else {
                        note = Note(null, ttl.value, tags.value, cnt.value)
                        db.noteDao().insertAll(note)
                    }
                    activity?.runOnUiThread {
                        router?.pop() //markdown(note.title!!, note.content!!)!!.launch()
                    }
                }

            }
        }
        return true
    }


}