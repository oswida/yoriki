package osw.yoriki.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.RouterPath
import com.google.android.material.floatingactionbutton.FloatingActionButton
import eu.davidea.flexibleadapter.FlexibleAdapter
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.DbWorker
import osw.yoriki.model.NoteItem
import osw.yoriki.model.NoteModel
import osw.yoriki.util.CnvUtils
import osw.yoriki.util.DialogUtils
import osw.yoriki.util.FileUtils

@RouterPath(name = "notelist")
class NotelistFragment : Fragment(), FlexibleAdapter.OnItemClickListener, FlexibleAdapter.OnItemSwipeListener {


    companion object {
        const val IMPORT_FILE_CODE = 200
    }

    private var recyclerView: RecyclerView? = null
    private lateinit var model: NoteModel
    private lateinit var fragmentAdapter: FlexibleAdapter<NoteItem>
    private val router: MoriRouter get() = (activity as MainActivity).router
    private var filter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.notelist_fragment, container, false)
        recyclerView = result.findViewById(R.id.list)
        model = ViewModelProviders.of(this).get(NoteModel::class.java)
        fragmentAdapter = FlexibleAdapter(null)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = fragmentAdapter
        }
        fragmentAdapter.isSwipeEnabled = true
        fragmentAdapter.addListener(this)
        model.itemList.observe(this, Observer<List<NoteItem>> {
            fragmentAdapter.updateDataSet(it)
        })
        val fab = result.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            router?.noteedit(-1L).launch()
        }
        refresh()
        return result
    }

    private fun refresh() {
        activity?.runOnUiThread {
            model.refresh(filter)
            activity?.title = "Notes"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.notelist_menu, menu)
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        if (!filter.isEmpty()) {
            searchView.setQuery(filter, false)
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                var tt = newText
                if (tt === null) {
                    tt = ""
                }
                filter = tt
                refresh()
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_import -> {
                val itn = Intent(Intent.ACTION_OPEN_DOCUMENT)
                itn.addCategory(Intent.CATEGORY_OPENABLE)
                itn.type = "*/*"
                startActivityForResult(itn, IMPORT_FILE_CODE)
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                IMPORT_FILE_CODE -> {
                    if (resultData!!.data != null) {
                        val data = FileUtils.readFile(context!!, resultData.data)
                        val note = CnvUtils.noteFromMarkdown(data)
                        DbWorker.postDbTask(context!!) { db ->
                            db.noteDao().insertAll(note!!)
                            model.refresh(filter)
                        }
                    }
                }
            }
        }
    }

    override fun onItemClick(view: View?, position: Int): Boolean {
        val item = fragmentAdapter.getItem(position)
        if (item != null) {
            router.markdown(item.item.title!!, item.item.content!!).launch()
        }
        return true
    }

    override fun onActionStateChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
    }

    override fun onItemSwipe(position: Int, direction: Int) {
        val item = fragmentAdapter.getItem(position)
        when (direction) {
            8 -> {
                DialogUtils.confirm(context!!, "Delete note", "Do you want to delete note ${item!!.item.title}?") {
                    DbWorker.postDbTask(context!!) { db ->
                        db.noteDao().delete(item.item)
                        model.refresh(filter)
                    }
                }
                model.refresh(filter)
            }
            4 -> {
                router.noteedit(item!!.item.id!!).launch()
            }
        }
    }

}