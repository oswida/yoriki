package osw.yoriki.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import com.squareup.moshi.Moshi
import com.thejuki.kformmaster.helper.FormBuildHelper
import com.thejuki.kformmaster.helper.form
import com.thejuki.kformmaster.helper.textArea
import com.thejuki.kformmaster.model.BaseFormElement
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.db.DbWorker
import osw.yoriki.db.RpgCharacter
import osw.yoriki.util.MiscUtils

enum class MdEditType {
    Unknown,
    CharNotes
}

@RouterPath(name = "markdownEdit")
class MarkdownEditFragment : Fragment() {

    val NOTES_TAG = 100

    @Argument
    var dataTitle: String = ""

    @Argument
    var data: String = ""

    @Argument
    var type: MdEditType = MdEditType.Unknown

    @Argument
    var moreinfo: Long = -1L

    private var recyclerView: RecyclerView? = null
    private val router: MoriRouter? get() = (activity as? MainActivity)?.router
    private var formBuilder: FormBuildHelper? = null
    private val moshi = Moshi.Builder().build()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val result = inflater.inflate(R.layout.markdown_edit, container, false)
        recyclerView = result.findViewById(R.id.formview)
        initForm()
        return result
    }

    fun initForm() {
        formBuilder = form(context!!, recyclerView!!) {
            textArea {
                displayTitle = false
                rightToLeft = false
                maxLines = 50
                value = data
                tag = NOTES_TAG
            }
        }
        activity?.title = dataTitle
        formBuilder!!.attachRecyclerView(context!!, recyclerView)
    }

    fun getElementValue(tag: Int): String {
        val element = formBuilder!!.getFormElement<BaseFormElement<String>>(tag)
        return element.value.toString()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.markdownedit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                when (type) {
                    MdEditType.CharNotes -> {
                        DbWorker.postDbTask(context!!) { db ->
                            val dchar = db.dcharDao().findById(moreinfo)
                            if (dchar != null) {
                                val adapter = moshi.adapter(RpgCharacter::class.java)
                                val rpc = adapter.fromJson(dchar.data)
                                rpc!!.notes = getElementValue(NOTES_TAG)
                                dchar.data = adapter.toJson(rpc)
                                db.dcharDao().save(dchar)
                                activity?.runOnUiThread {
                                    MiscUtils.hideKeyboard(context!!, view!!)
                                    router?.pop()
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }
}