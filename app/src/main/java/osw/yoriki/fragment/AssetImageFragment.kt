package osw.yoriki.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.github.chuross.morirouter.MoriBinder
import com.github.chuross.morirouter.annotation.Argument
import com.github.chuross.morirouter.annotation.RouterPath
import osw.yoriki.MainActivity
import osw.yoriki.R
import osw.yoriki.util.TextUtils
import java.io.File
import java.io.IOException

@RouterPath(name = "assetImage")
class AssetImageFragment : Fragment() {

    @Argument
    var path = ""

    private lateinit var imageView: SubsamplingScaleImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoriBinder.bind(this)
        setHasOptionsMenu(true)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val result = inflater.inflate(R.layout.image_fragment, container, false)
        imageView = result.findViewById(R.id.image)
        imageView.setOnLongClickListener {
            val bar = (activity as MainActivity).supportActionBar!!
            if (bar.isShowing) {
                bar.hide()
            } else {
                bar.show()
            }
            true
        }
        reloadImage()
        return result
    }

//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        inflater.inflate(R.menu.image_menu, menu)
//    }

    private fun reloadImage() {
        try {
            imageView.setImage(ImageSource.asset(path))
            activity?.title = TextUtils.removeImageExts(path.substring(path.lastIndexOf(File.separator) + 1))
        } catch (ex: IOException) {

        }
    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.action_rotate -> {
//                val rt = imageView.orientation
//                when (rt) {
//                    SubsamplingScaleImageView.ORIENTATION_0 -> imageView.orientation =
//                        SubsamplingScaleImageView.ORIENTATION_90
//                    SubsamplingScaleImageView.ORIENTATION_90 -> imageView.orientation =
//                        SubsamplingScaleImageView.ORIENTATION_180
//                    SubsamplingScaleImageView.ORIENTATION_180 -> imageView.orientation =
//                        SubsamplingScaleImageView.ORIENTATION_270
//                    else -> imageView.orientation = SubsamplingScaleImageView.ORIENTATION_0
//                }
//            }
//            R.id.action_edit -> {
//                router?.imageEdit(filename!!)!!.launch()
//            }
//        }
//        return true
//    }

}