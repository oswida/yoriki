package osw.yoriki.theme

import android.content.Context
import br.tiagohm.markdownview.css.styles.Github

class GithubLight(val context: Context, fontsize: Int = 14) : Github() {
    init {
        addRule(
            "body",
            "padding: 3px",
            "color: #333", "background-color: #fff",
            "font-size: $fontsize px"
        )
    }
}