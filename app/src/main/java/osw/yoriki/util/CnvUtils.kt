package osw.yoriki.util

import com.vladsch.flexmark.parser.Parser
import osw.yoriki.db.Note

object CnvUtils {

    enum class RingTags {
        EarthRing,
        WaterRing,
        AirRing,
        FireRing,
        VoidRing,
    }

    enum class AttrTags {
        Stamina,
        Willpower,
        Perception,
        Strength,
        Reflexes,
        Awareness,
        Agility,
        Intelligence,
        Magic
    }

    val attrtrans = mapOf(
        "Stamina" to "Wytrzymałość",
        "Willpower" to "Siła Woli",
        "Perception" to "Spostrzegawczość",
        "Strength" to "Siła",
        "Reflexes" to "Refleks",
        "Awareness" to "Intuicja",
        "Agility" to "Zręczność",
        "Intelligence" to "Inteligencja",
        "Magic" to "Magia"
    )

    enum class FightTags {
        Initiative,
        Attack,
        Damage,
        DeadWounds
    }

    val fighttrans = mapOf(
        "Initiative" to "Inicjatywa",
        "Attack" to "Atak",
        "Damage" to "Obrażenia",
        "DeadWounds" to "Pkt obrażeń",
        "Body Points" to "Pkt obrażeń"
    )

    enum class SocialTags {
        Honor,
        Glory,
        Status
    }

    val soctrans = mapOf(
        "Honor" to "Honor",
        "Glory" to "Chwała",
        "Status" to "Status"
    )

    enum class ShadowTags {
        Fear,
        Taint
    }

    val shadowtrans = mapOf(
        "Fear" to "Strach",
        "Taint" to "Skaza"
    )

    val battleSkills = listOf(
        "jiujutsu",
        "weapons:chain",
        "weapons:heavy",
        "weapons:kenjutsu",
        "weapons:knives",
        "weapons:polearms",
        "weapons:spears",
        "weapons:staves",
        "weapons:fan",
        "iaijutsu",
        "kyujutsu",
        "ninjutsu",
        "defense"
    )

    val skills = mapOf(
        AttrTags.Stamina to listOf(),
        AttrTags.Willpower to listOf(
            "meditation",
            "tea_ceremony",
            "games",
            "perform",
            "perform:biwa",
            "perform:dance",
            "perform:drums",
            "perform:flute",
            "perform:oratory",
            "perform:puppeteer",
            "perform:samisen",
            "perform:song",
            "perform:storytelling"
        ),
        AttrTags.Perception to listOf("investigation", "battle", "hunting"),
        AttrTags.Strength to listOf("athletics"),
        AttrTags.Reflexes to listOf("defense", "iaijutsu", "kyujutsu", "ninjutsu"),
        AttrTags.Awareness to listOf(
            "acting",
            "artisan",
            "artisan:bonsai",
            "artisan:gardening",
            "artisan:origami",
            "artisan:ikebana",
            "artisan:painting",
            "artisan:poetry",
            "artisan:sculpture",
            "artisan:tatooing",
            "courtier",
            "etiquette",
            "sincerity",
            "animal_handling",
            "intimidation",
            "temptation"
        ),
        AttrTags.Agility to listOf(
            "horsemanship",
            "jiujutsu",
            "weapons",
            "weapons:chain",
            "weapons:heavy",
            "weapons:kenjutsu",
            "weapons:knives",
            "weapons:polearms",
            "weapons:spears",
            "weapons:staves",
            "weapons:fan",
            "forgery",
            "sleight_of_hand",
            "stealth",
            "craft",
            "craft:armorsmithing",
            "craft:blacksmithing",
            "craft:brewing",
            "craft:carpentry",
            "craft:cartography",
            "craft:cobbling",
            "craft:cooking",
            "craft:farming",
            "craft:fishing",
            "craft:masonry",
            "craft:mining",
            "craft:poison",
            "craft:pottery",
            "craft:shipbuilding",
            "craft:tailoring",
            "craft:weaponsmithing",
            "craft:weaving"
        ),
        AttrTags.Intelligence to listOf(
            "calligraphy",
            "divination",
            "lore",
            "lore:anatomy",
            "lore:architecture",
            "lore:bushido",
            "lore:clan",
            "lore:elements",
            "lore:gaijin",
            "lore:ghosts",
            "lore:heraldry",
            "lore:history",
            "lore:maho",
            "lore:nature",
            "lore:nonhuman",
            "lore:omens",
            "lore:shadowlands",
            "lore:shugenja",
            "lore:spirit_realms",
            "lore:theology",
            "lore:underworld",
            "lore:war",
            "medicine",
            "spellcraft:air",
            "spellcraft:water",
            "spellcraft:earth",
            "spellcraft:fire",
            "spellcraft:void",
            "commerce",
            "engineering",
            "sailing"
        )
    )

    val skilltrans = mapOf(
        "acting" to "Aktorstwo",
        "artisan" to "Sztuka",
        "artisan:bonsai" to "Sztuka:Bonsai",
        "artisan:gardening" to "Sztuka:Ogrodnictwo",
        "artisan:origami" to "Sztuka:Origami",
        "artisan:ikebana" to "Sztuka:Ikebana",
        "artisan:painting" to "Sztuka:Malowanie",
        "artisan:poetry" to "Sztuka:Poezja",
        "artisan:sculpture" to "Sztuka:Rzeźba",
        "artisan:tatooing" to "Sztuka:Tatuaże",
        "calligraphy" to "Kaligrafia",
        "courtier" to "Dyplomacja",
        "divination" to "Wróżbiarstwo",
        "etiquette" to "Etykieta",
        "games" to "Hazard",
        "investigation" to "Zdolności śledcze",
        "lore" to "Wiedza",
        "lore:anatomy" to "Wiedza:Anatomia",
        "lore:architecture" to "Wiedza:Architektura",
        "lore:bushido" to "Wiedza:Bushido",
        "lore:clan" to "Wiedza:Klan",
        "lore:elements" to "Wiedza:Żywioły",
        "lore:gaijin" to "Wiedza:Gaijin",
        "lore:ghosts" to "Wiedza:Duchy",
        "lore:heraldry" to "Wiedza:Heraldyka",
        "lore:history" to "Wiedza:Historia",
        "lore:maho" to "Wiedza:Maho",
        "lore:nature" to "Wiedza:Natura",
        "lore:nonhuman" to "Wiedza:Nieludzkie rasy",
        "lore:omens" to "Wiedza:Omeny",
        "lore:shadowlands" to "Wiedza:Krainy Cienia",
        "lore:shugenja" to "Wiedza:Shugenja",
        "lore:spirit_realms" to "Wiedza:Światy Duchów",
        "lore:theology" to "Wiedza:Teologia",
        "lore:underworld" to "Wiedza:Półświatek",
        "lore:war" to "Wiedza:Wojna",
        "medicine" to "Medycyna",
        "meditation" to "Medytacja",
        "perform" to "Występy",
        "perform:biwa" to "Występy:Biwa",
        "perform:dance" to "Występy:Taniec",
        "perform:drums" to "Występy:Bębny",
        "perform:flute" to "Występy:Flet",
        "perform:oratory" to "Występy:Przemowy",
        "perform:puppeteer" to "Występy:Lalkarstwo",
        "perform:samisen" to "Występy:Samisen",
        "perform:song" to "Występy:Śpiew",
        "perform:storytelling" to "Występy:Opowiadanie",
        "sincerity" to "Szczerość",
        "spellcraft:air" to "Magia:Powietrze",
        "spellcraft:water" to "Magia:Woda",
        "spellcraft:earth" to "Magia:Ziemia",
        "spellcraft:fire" to "Magia:Ogień",
        "spellcraft:void" to "Magia:Pustka",
        "tea_ceremony" to "Ceremonia herbaciana",
        "athletics" to "Atletyka",
        "battle" to "Sztuka Wojny",
        "defense" to "Obrona",
        "horsemanship" to "Jeździectwo",
        "hunting" to "Polowanie",
        "iaijutsu" to "Iaijutsu",
        "jiujutsu" to "Walka wręcz",
        "weapons" to "Walka bronią białą",
        "kyujutsu" to "Strzelanie z łuku",
        "weapons:chain" to "Walka bronią białą:Łańcuchowe",
        "weapons:heavy" to "Walka bronią białą:Ciężkie",
        "weapons:kenjutsu" to "Walka bronią białą:Kenjutsu",
        "weapons:knives" to "Walka bronią białą:Noże",
        "weapons:polearms" to "Walka bronią białą:Drzewcowe",
        "weapons:spears" to "Walka bronią białą:Włócznie",
        "weapons:staves" to "Walka bronią białą:Pałki",
        "weapons:fan" to "Walka bronią białą:Wachlarz",
        "animal_handling" to "Opieka nad zwierzętami",
        "ninjutsu" to "Ninjutsu",
        "commerce" to "Handel",
        "craft" to "Rzemiosło",
        "craft:armorsmithing" to "Rzemiosło:Płatnerstwo",
        "craft:blacksmithing" to "Rzemiosło:Kowalstwo",
        "craft:brewing" to "Rzemiosło:Warzelnictwo",
        "craft:carpentry" to "Rzemiosło:Stolarstwo",
        "craft:cartography" to "Rzemiosło:Kartografia",
        "craft:cobbling" to "Rzemiosło:Brukowanie",
        "craft:cooking" to "Rzemiosło:Gotowanie",
        "craft:farming" to "Rzemiosło:Uprawa",
        "craft:fishing" to "Rzemiosło:Rybołówstwo",
        "craft:masonry" to "Rzemiosło:Kamieniarstwo",
        "craft:mining" to "Rzemiosło:Górnictwo",
        "craft:poison" to "Rzemiosło:Trucizny",
        "craft:pottery" to "Rzemiosło:Garncarstwo",
        "craft:shipbuilding" to "Rzemiosło:Szkutnictwo",
        "craft:tailoring" to "Rzemiosło:Krawiectwo",
        "craft:weaponsmithing" to "Rzemiosło:Wyrób broni",
        "craft:weaving" to "Rzemiosło:Tkactwo",
        "engineering" to "Inżynieria",
        "sailing" to "Żeglarstwo",
        "forgery" to "Fałszerstwo",
        "intimidation" to "Zastraszanie",
        "sleight_of_hand" to "Kradzież",
        "temptation" to "Kuszenie",
        "stealth" to "Ukrywanie się"
    )

    val misctrans = mapOf(
        "Attributes" to "Atrybuty",
        "Skills" to "Umiejętności",
        "Equipment" to "Ekwipunek",
        "Notes" to "Notatki",
        "Extras" to "Własności dodatkowe",
        "Strength Dmg" to "Obr. od Siły",
        "Void Points" to "Pkt Pustki",
        "Spells" to "Zaklęcia"
    )


    fun diceFromString(text: String): Int {
        if (text.isBlank()) {
            return 0
        }
        val idx = text.toUpperCase().indexOf('K')
        if (idx > 0) {
            return text.substring(0, idx).toIntOrZero()
        }
        return text.toIntOrZero()
    }

    fun cnvRollKeep(roll: Int, keep: Int): String {
        var dice = keep
        var pip = roll / 2
        if (pip > 2) {
            val diceplus = pip / 3
            val dicerest = pip % 3
            dice += diceplus
            pip = dicerest
        }
        if (pip > 0) {
            return "${dice}K+${pip}"
        } else {
            return "${dice}K"
        }
    }

    fun cnvRollString(roll: String): String {
        val text = roll.toUpperCase().split('K')
        if (text.size == 2) {
            return cnvRollKeep(text[0].toInt(), text[1].toInt())
        }
        return ""
    }

    fun attrString(attr: Int, ring: Int): String {
        if (attr == 0) {
            return "${ring}K"
        } else {
            return "${attr}K"
        }
    }

    fun findAttrForSkill(skill: String): AttrTags? {
        skills.keys.forEach {
            val list = skills[it]
            if (list!!.contains(skill)) {
                return it
            }
        }
        return null
    }

    fun skillString(skill: Int, attr: Int): String {
        return cnvRollKeep(attr + skill, attr)
    }

    fun valueIsRollKeep(value: String): Boolean {
        val regex = Regex("[0-9]+[kK][0-9]+")
        return regex.matchEntire(value) != null
    }


    fun noteFromMarkdown(text: String): Note? {
        val parser = Parser.Builder().build()
        val doc = parser.parse(text)
        var title = ""
        var text = ""
        val tags = mutableListOf<String>()
        val tagreg = Regex("@[a-zA-Z0-9_]+")
        doc.children.forEach {
            if (it.nodeName == "Heading" && title.isEmpty()) {
                title = it.chars.toString().replaceFirst("#", "").trim()
            }
            text += it.chars.toString() + "\n"
        }
        val matches = tagreg.findAll(text)
        matches.forEach {
            tags.add(it.value)
        }
        val result = Note(null, title, tags.joinToString(" "), text)
        return result
    }

    infix fun trans(text: String): String {
        var result = CnvUtils.attrtrans[text]
        if (result != null) {
            return result
        }
        result = CnvUtils.fighttrans[text]
        if (result != null) {
            return result
        }
        result = CnvUtils.soctrans[text]
        if (result != null) {
            return result
        }
        result = CnvUtils.shadowtrans[text]
        if (result != null) {
            return result
        }
        result = CnvUtils.skilltrans[text]
        if (result != null) {
            return result
        }
        result = CnvUtils.misctrans[text]
        if (result != null) {
            return result
        }
        return text
    }

}

fun String.toIntOrZero(): Int {
    val reg = Regex("[-]?[0-9]+")
    if (reg.matchEntire(this) != null) {
        return this.toInt()
    }
    return 0
}

fun String.toFloatOrZero(): Float {
    val reg = Regex("[-]?[0-9]+\\.[0-9]+")
    if (reg.matchEntire(this) != null) {
        return this.toFloat()
    }
    return 0.0f
}