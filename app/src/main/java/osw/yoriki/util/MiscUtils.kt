package osw.yoriki.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import osw.yoriki.db.DbWorker

object MiscUtils {

    val charActions = listOf("Atak", "Atak drugą bronią", "Obrona", "Czekanie")

    fun loadStandardNotes(context: Context) {
        val files = context.assets.list("notes")
        DbWorker.postDbTask(context) { db ->
            files.forEach { filename ->
                val text = FileUtils.textFromAsset(context.assets, "notes/$filename")
                val note = CnvUtils.noteFromMarkdown(text)
                if (note != null) {
                    val found = db.noteDao().findByTitle(note.title!!)
                    if (found == null) {
                        db.noteDao().insertAll(note)
                    }
                }
            }
        }
    }

    fun hideKeyboard(context: Context, view: View) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


}