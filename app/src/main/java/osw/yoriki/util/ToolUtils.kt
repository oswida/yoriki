package osw.yoriki.util

import android.content.Context
import android.view.LayoutInflater
import android.widget.NumberPicker
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onPreShow
import com.afollestad.materialdialogs.customview.customView
import osw.yoriki.R

enum class TaskDiffCode {
    VeryEasy,
    Easy,
    Moderate,
    Difficult,
    VeryDifficult,
    Heroic,
    Unknown
}


object ToolUtils {

    val diffMem = mapOf(
        "VE" to TaskDiffCode.VeryEasy,
        "E" to TaskDiffCode.Easy,
        "M" to TaskDiffCode.Moderate,
        "D" to TaskDiffCode.Difficult,
        "VD" to TaskDiffCode.VeryDifficult,
        "H" to TaskDiffCode.Heroic,
        "U" to TaskDiffCode.Unknown
    )

    val diffToBeat = mapOf(
        "VE" to 5,
        "E" to 10,
        "M" to 15,
        "D" to 20,
        "VD" to 30,
        "H" to 31,
        "U" to 100
    )

    val commandTable = mapOf(
        1 to listOf("M", "M", "E", "E", "E", "VE", "VE", "VE", "1"),
        2 to listOf("M", "M", "E", "E", "E", "VE", "VE", "VE", "1"),
        3 to listOf("D", "M", "M", "M", "E", "E", "VE", "VE", "2"),
        4 to listOf("D", "M", "M", "M", "E", "E", "VE", "VE", "2"),
        5 to listOf("D", "D", "M", "M", "M", "M", "E", "E", "3"),
        6 to listOf("D", "D", "M", "M", "M", "M", "E", "E", "3"),
        7 to listOf("D", "D", "D", "M", "M", "M", "E", "E", "4"),
        8 to listOf("D", "D", "D", "M", "M", "M", "E", "E", "4"),
        9 to listOf("D", "D", "D", "M", "M", "M", "E", "E", "4"),
        10 to listOf("D", "D", "D", "M", "M", "M", "E", "E", "4"),
        11 to listOf("VD", "D", "D", "D", "M", "M", "M", "E", "5"),
        12 to listOf("VD", "D", "D", "D", "M", "M", "M", "E", "5"),
        13 to listOf("VD", "D", "D", "D", "M", "M", "M", "E", "5"),
        14 to listOf("VD", "D", "D", "D", "M", "M", "M", "E", "5"),
        15 to listOf("VD", "D", "D", "D", "M", "M", "M", "E", "5"),
        16 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        17 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        18 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        19 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        20 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        21 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        22 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        23 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        24 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        25 to listOf("VD", "VD", "D", "D", "D", "M", "M", "M", "6"),
        40 to listOf("VD", "VD", "VD", "D", "D", "D", "M", "M", "7"),
        60 to listOf("H", "VD", "VD", "VD", "D", "D", "D", "M", "8")
    )

    fun rollResultDiff(roll: Int): TaskDiffCode {
        if (roll in 1..5) {
            return TaskDiffCode.VeryEasy
        } else if (roll in 6..10) {
            return TaskDiffCode.Easy
        } else if (roll in 11..15) {
            return TaskDiffCode.Moderate
        } else if (roll in 16..20) {
            return TaskDiffCode.Difficult
        } else if (roll in 21..30) {
            return TaskDiffCode.VeryDifficult
        } else if (roll >= 31) {
            return TaskDiffCode.Heroic
        }
        return TaskDiffCode.Unknown
    }

    fun rollBeatsTaskDiff(roll: Int, diff: TaskDiffCode): Boolean {
        val rollDiff = rollResultDiff(roll)
        if (rollDiff == TaskDiffCode.Unknown) {
            return false
        }
        return rollDiff.ordinal > diff.ordinal
    }

    fun coopCalculatorTool(context: Context) {
        val layout = LayoutInflater.from(context).inflate(R.layout.tool_command_calc, null)
        val people = layout.findViewById<NumberPicker>(R.id.people)
        people.minValue = 1
        people.maxValue = 25
        people.value = 1
        val skill = layout.findViewById<NumberPicker>(R.id.skill)
        skill.minValue = 1
        skill.maxValue = 8
        skill.value = 1
        val level = layout.findViewById<TextView>(R.id.level)
        val rolled = layout.findViewById<NumberPicker>(R.id.rolled)
        val info = layout.findViewById<TextView>(R.id.info)
        people.setOnValueChangedListener { picker, oldVal, newVal ->
            val lvl = commandTable[people.value]!![skill.value - 1]
            val num = diffToBeat[lvl]
            level.text = "$num \n$lvl"
            rolled.value = 1
            info.text = ""
        }
        skill.setOnValueChangedListener { picker, oldVal, newVal ->
            val lvl = commandTable[people.value]!![skill.value - 1]
            val num = diffToBeat[lvl]
            level.text = "$num \n$lvl"
            rolled.value = 1
            info.text = ""
        }
        rolled.minValue = 1
        rolled.maxValue = 100
        rolled.setOnValueChangedListener { picker, oldVal, newVal ->
            if (level.text.isNotBlank()) {
                val code = level.text.split("\n")[1]
                if (rolled.value > diffToBeat[code]!!) {
                    info.text = "Success\n${people.value} persons\nbonus: +${commandTable[people.value]!![8]}K"
                } else {
                    var cnt = people.value - 1
                    while (cnt > 0) {
                        val lvl = commandTable[cnt]!![skill.value - 1]
                        if (rolled.value > diffToBeat[lvl]!!) {
                            info.text = "${cnt} persons\nbonus: +${commandTable[cnt]!![8]}K"
                            break
                        }
                        cnt--
                    }
                    if (cnt <= 0) {
                        info.text = "Total failure"
                    }
                }
            }
        }
        MaterialDialog(context).show {
            title(text = "Cooperation calc")
            customView(view = layout)
            negativeButton(text = "Close")
        }
    }


    fun spellDifficultyTool(context: Context) {
        val layout = LayoutInflater.from(context).inflate(R.layout.dlg_lvl_convert, null)
        val picker = layout.findViewById<NumberPicker>(R.id.lvl)
        val result = layout.findViewById<TextView>(R.id.result)
        picker.minValue = 1
        picker.maxValue = 10
        picker.setOnValueChangedListener { picker, oldVal, newVal ->
            val computed = 5 + 5 * picker.value
            result.text = l5klevels[computed].toString()
        }
        MaterialDialog(context).show {
            title(text = "Set spell mastery")
            customView(view = layout)
            negativeButton(text = "Close")
        }
    }

    val l5klevels = mapOf(
        5 to 1, 15 to 11, 25 to 16, 35 to 23, 45 to 27, 55 to 29,
        6 to 2, 16 to 11, 26 to 17, 36 to 24, 46 to 27, 56 to 30,
        7 to 3, 17 to 12, 27 to 18, 37 to 24, 47 to 27, 57 to 30,
        8 to 4, 18 to 12, 28 to 19, 38 to 25, 48 to 28, 58 to 30,
        9 to 5, 19 to 13, 29 to 20, 39 to 25, 49 to 28, 59 to 30,
        10 to 6, 20 to 13, 30 to 21, 40 to 26, 50 to 28, 60 to 31,
        11 to 7, 21 to 14, 31 to 21, 41 to 26, 51 to 28,
        12 to 8, 22 to 14, 32 to 22, 42 to 26, 52 to 29,
        13 to 9, 23 to 15, 33 to 22, 43 to 26, 53 to 29,
        14 to 10, 24 to 15, 34 to 23, 44 to 27, 54 to 29
    )

    fun L5KLvlConvert(context: Context) {
        val layout = LayoutInflater.from(context).inflate(R.layout.dlg_lvl_convert, null)
        val picker = layout.findViewById<NumberPicker>(R.id.lvl)
        val result = layout.findViewById<TextView>(R.id.result)
        picker.minValue = 5
        picker.maxValue = 60
        picker.value = 5
        result.text = l5klevels[picker.value].toString()
        picker.setOnValueChangedListener { picker, oldVal, newVal ->
            result.text = l5klevels[picker.value].toString()
        }
        MaterialDialog(context).show {
            title(text = "L5K Level Conversion")
            negativeButton(text = "Close")
            customView(view = layout)
        }
    }

    fun L5KRollConvert(context: Context) {
        val layout = LayoutInflater.from(context).inflate(R.layout.dlg_roll_convert, null)
        val pickerRoll = layout.findViewById<NumberPicker>(R.id.roll)
        val pickerKeep = layout.findViewById<NumberPicker>(R.id.keep)
        val result = layout.findViewById<TextView>(R.id.result)
        pickerRoll.minValue = 0
        pickerRoll.maxValue = 10
        pickerRoll.value = 0
        pickerKeep.minValue = 0
        pickerKeep.maxValue = 10
        pickerKeep.value = 0
        result.text = ""
        pickerRoll.setOnValueChangedListener { picker, oldVal, newVal ->
            result.text = CnvUtils.cnvRollKeep(newVal, pickerKeep.value)
        }
        pickerKeep.setOnValueChangedListener { picker, oldVal, newVal ->
            result.text = CnvUtils.cnvRollKeep(pickerRoll.value, newVal)
        }
        MaterialDialog(context).show {
            title(text = "L5K Roll Conversion")
            negativeButton(text = "Close")
            customView(view = layout)
        }.onPreShow { MiscUtils.hideKeyboard(context, layout) }
    }

}