package osw.yoriki.util

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.widget.*
import br.tiagohm.markdownview.MarkdownView
import br.tiagohm.markdownview.css.styles.Github
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onPreShow
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.files.fileChooser
import com.afollestad.materialdialogs.files.folderChooser
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItems
import com.afollestad.materialdialogs.list.listItemsMultiChoice
import osw.yoriki.R
import osw.yoriki.theme.GithubDark
import osw.yoriki.theme.GithubLight
import java.io.File

object DialogUtils {



    fun confirm(context: Context, title: String, message: String, func: () -> Unit) {
        MaterialDialog(context).show {
            title(text = title)
            message(text = message)
            positiveButton(text = "Confirm") {
                func()
            }
            negativeButton(text = "Cancel")
        }
    }

    fun info(context: Context, title: String, message: String) {
        MaterialDialog(context).show {
            title(text = title)
            message(text = message)
            negativeButton(text = "Close")
        }
    }

    fun mdinfo(context: Context, title: String, message: String) {
        val mdview = MarkdownView(context)
        var theme: Github = GithubLight(context)
        if (PrefUtils.isNightmode(context!!)) {
            theme = GithubDark(context!!)
        }
        mdview.addStyleSheet(theme)
        mdview.loadMarkdown(message)
        MaterialDialog(context).show {
            title(text = title)
            customView(view = mdview)
            negativeButton(text = "Close")
        }
    }

    fun select(context: Context, title: String, list: List<String>, func: (Int, String) -> Unit) {
        if (title.isNotBlank()) {
            MaterialDialog(context).show {
                title(text = title)
                listItems(items = list) { dialog, index, text ->
                    func(index, text)
                }
            }
        } else {
            MaterialDialog(context).show {
                listItems(items = list) { dialog, index, text ->
                    func(index, text)
                }
            }
        }
    }

    fun selectWithSearch(context: Context, title: String, list: List<String>, func: (Int, String) -> Unit) {
        val layout = LayoutInflater.from(context).inflate(R.layout.dlg_list_search, null)
        val search = layout.findViewById<EditText>(R.id.search)
        val listView = layout.findViewById<ListView>(R.id.list)
        listView.adapter = ArrayAdapter<String>(
            context,
            android.R.layout.simple_list_item_1, list
        )
        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isBlank()) {
                    listView.adapter = ArrayAdapter<String>(
                        context,
                        android.R.layout.simple_list_item_1, list
                    )
                } else {
                    listView.adapter = ArrayAdapter<String>(
                        context,
                        android.R.layout.simple_list_item_1,
                        list.filter { it.toLowerCase().startsWith(s.toString().toLowerCase()) }
                    )
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        listView.setOnItemClickListener { parent, view, position, id ->
            func(position, list[position])
        }
        MaterialDialog(context).show {
            title(text = title)
            customView(view = layout)
            negativeButton (text = "Close") {
                dismiss()
            }
        }
    }

    fun selectInt(
        context: Context, title: String, minValue: Int,
        maxValue: Int, value: Int, func: (Int) -> Unit
    ) {
        val layout = LayoutInflater.from(context).inflate(R.layout.dlg_number_picker, null)
        val picker = layout.findViewById<NumberPicker>(R.id.picker)
        picker.minValue = minValue
        picker.maxValue = maxValue
        picker.value = value
        MaterialDialog(context).show {
            title(text = title)
            customView(view = layout)
            negativeButton(text = "Cancel")
            positiveButton(text = "Select") {
                func(picker.value)
            }
        }
    }

    fun inputInt(context: Context, title: String, value: Int, func: (Int) -> Unit) {
        MaterialDialog(context).show {
            title(text = title)
            input(
                prefill = value.toString(),
                inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED
            ) { _, value ->
                func(value.toString().toIntOrZero())
            }
            negativeButton(text = "Cancel")
        }
    }

    fun inputString(context: Context, title: String, value: String, func: (String) -> Unit) {
        MaterialDialog(context).show {
            title(text = title)
            input(
                prefill = value,
                allowEmpty = true
            ) { _, value ->
                func(value.toString())
            }
            negativeButton(text = "Cancel")
        }
    }

    fun selectMultiple(
        context: Context,
        title: String,
        values: List<String>,
        selected: IntArray,
        func: (List<String>) -> Unit
    ) {
        MaterialDialog(context).show {
            title(text = title)
            val list = mutableListOf<String>()
            listItemsMultiChoice(
                items = values,
                initialSelection = selected,
                waitForPositiveButton = true,
                allowEmptySelection = true
            ) { dialog, indices, items ->
                func(items)
            }
            positiveButton { }
        }
    }

    fun selectFolder(context: Context, title: String, func:(File)->Unit) {
        MaterialDialog(context).show {
            folderChooser(allowFolderCreation = true) { dialog, folder ->
                func(folder)
            }
        }
    }

    fun selectFile(context: Context, title: String, func:(File)->Unit) {
        MaterialDialog(context).show {
            fileChooser (allowFolderCreation = true) { dialog, file ->
                func(file)
            }
        }
    }

}