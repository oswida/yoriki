package osw.yoriki.util

import android.content.Context
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatDelegate

object PrefUtils {
    val PREF_NIGHTMODE = "pref_nightmode"


    fun getText(context: Context, name: String, defValue: String = ""): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(name, defValue)
    }

    fun getInt(context: Context, name: String, defValue: Int = 0): Int {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getInt(name, defValue)
    }

    fun getFloat(context: Context, name: String, defValue: Float = 0f): Float {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getFloat(name, defValue)
    }

    fun getBoolean(context: Context, name: String, defValue: Boolean = false): Boolean {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getBoolean(name, defValue)
    }

    fun setText(context: Context, name: String, value: String) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        sharedPreferences.edit().putString(name, value).apply()
    }

    fun setInt(context: Context, name: String, value: Int) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        sharedPreferences.edit().putInt(name, value).apply()
    }

    fun setFloat(context: Context, name: String, value: Float) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        sharedPreferences.edit().putFloat(name, value).apply()
    }

    fun setBoolean(context: Context, name: String, value: Boolean) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        sharedPreferences.edit().putBoolean(name, value).apply()
    }

    fun getNightmode(context: Context): Int {
        if (getBoolean(context, PREF_NIGHTMODE, false)) {
            return AppCompatDelegate.MODE_NIGHT_YES
        } else {
            return AppCompatDelegate.MODE_NIGHT_NO
        }
    }

    fun isNightmode(context: Context): Boolean {
        return getBoolean(context, PREF_NIGHTMODE, false)
    }
}