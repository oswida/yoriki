package osw.yoriki.util

import android.content.Context
import android.graphics.Typeface
import com.ajts.androidmads.fontutils.FontUtils


object TextUtils {

    val fontUtils = FontUtils()
    var l5rfonts: Typeface? = null

    fun initFonts(context: Context) {
        l5rfonts = Typeface.createFromAsset(context.assets, "fonts/L5RFont.ttf")
//        fontUtils.applyFontToToolbar(toolbar, TextUtils.l5rfonts)
//        fontUtils.applyFontToNavigationView(nav_view, TextUtils.l5rfonts)
//        fontUtils.applyFontToMenu(menu, TextUtils.l5rfonts)
    }


    fun removeImageExts(text: String): String {
        return text.replace(".jpg", "")
            .replace(".png", "")
            .replace(".webp", "")
            .replace(".jpeg", "")
    }

    fun removeJsonExts(text: String): String {
        return text.replace(".json", "")
            .replace(".yaml", "")
    }

    fun mdTableStart(vararg headers: String): String {
        var text = "|" + headers.joinToString("|") + "|\n"
        text += "|" + headers.map { ":---:" }.joinToString("|") + "|\n"
        return text
    }

    fun mdTableRow(vararg cols: String): String {
        return "|" + cols.joinToString("|") + "|\n"
    }
}

fun String.spacePadded(): String {
    return "  $this  "
}