package osw.yoriki.util

import android.content.Context
import android.content.res.AssetManager
import android.net.Uri
import java.io.*
import java.nio.channels.FileChannel

object FileUtils {

    @Throws(IOException::class)
    fun textFromAsset(assetManager: AssetManager, path: String): String {
        val inputStream = assetManager.open(path)
        val reader = BufferedReader(InputStreamReader(inputStream))
        val stringBuilder = StringBuilder()
        var line = reader.readLine()
        while (line != null) {
            stringBuilder.append(line).append("\n")
            line = reader.readLine()
        }
        reader.close()
        inputStream.close()
        return stringBuilder.toString()
    }

    fun readFile(context: Context?, uri: Uri): String {
        val inputStream = context!!.contentResolver.openInputStream(uri)
        val reader = InputStreamReader(inputStream)
        val result = reader.readText()
        reader.close()
        return result
    }

    @Throws(IOException::class)
    fun copyFile(fromFile: FileInputStream, toFile: FileOutputStream) {
        var fromChannel: FileChannel? = null
        var toChannel: FileChannel? = null
        try {
            fromChannel = fromFile.getChannel()
            toChannel = toFile.getChannel()
            fromChannel!!.transferTo(0, fromChannel!!.size(), toChannel)
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel!!.close()
                }
            } finally {
                if (toChannel != null) {
                    toChannel!!.close()
                }
            }
        }
    }


}