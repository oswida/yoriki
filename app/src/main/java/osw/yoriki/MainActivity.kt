package osw.yoriki

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.transition.Fade
import com.github.chuross.morirouter.MoriRouter
import com.github.chuross.morirouter.core.DefaultTransitionFactory
import com.github.chuross.morirouter.core.MoriRouterOptions
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import osw.yoriki.db.AppDatabase
import osw.yoriki.db.DbWorker
import osw.yoriki.fragment.SettingsFragment
import osw.yoriki.util.DialogUtils
import osw.yoriki.util.FileUtils
import osw.yoriki.util.MiscUtils
import osw.yoriki.util.PrefUtils
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        const val MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 150
    }


    lateinit var router: MoriRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        DbWorker.getInstance()!!.start()

        nav_view.setNavigationItemSelectedListener(this)
        val transitionFactory = DefaultTransitionFactory { Fade() }
        val options = MoriRouterOptions.Builder(R.id.container)
            .setEnterTransitionFactory(transitionFactory)
            .setExitTransitionFactory(transitionFactory)
            .build()
        router = MoriRouter(supportFragmentManager, options)
        MiscUtils.loadStandardNotes(this)
        switchNightmode()
        checkPermissions()
        router.notelist().launch()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        DbWorker.getInstance()?.quit()
        DbWorker.destroyInstance()
        AppDatabase.getInstance(this)?.close()
        AppDatabase.destroyInstance()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> openSettings()
            R.id.action_exportdb -> exportDb()
            R.id.action_importdb -> importDb()
            R.id.action_tools -> router.tools().launch()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    fun exportDb() {
        val dbFile = File(this.getDatabasePath("yoriki.db").absolutePath)
        DialogUtils.selectFolder(this, "Database export folder") {
            val format = SimpleDateFormat("yyyy-MM-dd")
            val outfile = File(it.absolutePath + File.separator + "yoriki-" + format.format(Date()) + ".db")
            FileUtils.copyFile(FileInputStream(dbFile), FileOutputStream(outfile))
            DialogUtils.info(this, "Export database", "Database exported")
        }
    }

    fun importDb() {
        val dbFile = File(this.getDatabasePath("yoriki.db").absolutePath)
        DialogUtils.confirm(this, "Import database", "Are you sure? All database content will be overwritten!") {
            DialogUtils.selectFile(this, "Database import") {
                AppDatabase.getInstance(this)?.close()
                AppDatabase.destroyInstance()
                dbFile.delete()
                FileUtils.copyFile(FileInputStream(it), FileOutputStream(dbFile))
                DialogUtils.info(this, "Import database", "Database imported")
                router.notelist().launch()
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_maps -> {
                router.maplist().launch()
            }
            R.id.nav_spells -> {
                router.spelllist().launch()
            }
            R.id.nav_chars -> {
                router.charlist().launch()
            }
            R.id.nav_notes -> {
                router.notelist().launch()
            }
            R.id.nav_tools -> router.tools().launch()
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun switchNightmode() {
        AppCompatDelegate.setDefaultNightMode(PrefUtils.getNightmode(this))
    }

    fun openSettings() {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val settings = SettingsFragment()
        fragmentTransaction.replace(R.id.container, settings)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
                )
            }
        }
    }
}
