package osw.yoriki.util

import junit.framework.Assert.assertEquals
import org.junit.Test

class CnvUtilsTest {

    @Test
    fun testRollKeep() {
        var result = CnvUtils.cnvRollKeep(5,4)
        assertEquals("4K+2", result)
        result = CnvUtils.cnvRollKeep(3,3)
        assertEquals("3K+1", result)
        result = CnvUtils.cnvRollKeep(7,2)
        assertEquals("3K", result)
        result = CnvUtils.cnvRollKeep(10,2)
        assertEquals("3K+2", result)
    }
}